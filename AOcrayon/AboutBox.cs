﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.Diagnostics;
using System.Windows.Forms;
using Twp.Controls.HtmlRenderer.Entities;

namespace AOcrayon
{
    /// <summary>
    /// Description of AboutBox.
    /// </summary>
    public partial class AboutBox : UserControl
    {
        public AboutBox()
        {
            InitializeComponent();
            this.pictureBox.Image = Properties.Resources.Splash;
            this.htmlLabel.BaseStylesheet = Properties.Resources.BaseCSS;
            this.htmlLabel.Text = Properties.Resources.AboutText;
        }

        internal void OnLinkClicked( object sender, HtmlLinkClickedEventArgs e )
        {
            Process.Start( e.Link );
        }
    }
}
