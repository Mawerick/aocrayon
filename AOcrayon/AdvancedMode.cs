﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using FCGame;

namespace AOcrayon
{
    /// <summary>
    /// Description of AdvancedMode.
    /// </summary>
    public partial class AdvancedMode : UserControl
    {
        public AdvancedMode()
        {
            InitializeComponent();
            this.advancedHelpLabel.BaseStylesheet = Properties.Resources.BaseCSS;
        }

        public void Init()
        {
            Program.Settings.BackgroundColorChanged += new EventHandler( OnBackgroundColorChanged );
            Program.GameMode.Colors.ListChanged += new ListChangedEventHandler( OnColorsChanged );
            Program.GameMode.Colors.LoadFinished += new EventHandler( OnColorsLoaded );
            
            this.advancedHelpLabel.Text = Program.GameMode.Resources.GetString( "AdvancedHelp", Application.CurrentCulture );
            this.colorList.VirtualListSize = Program.GameMode.Colors.Count;
            if( Program.GameMode.Colors.HasFonts )
            {
                this.setFontButton.Visible = true;
                this.setFontButton.Enabled = false;
                this.setColorButton.Enabled = false;
                this.setColorButton.Width = this.setFontButton.Left - 6;
            }
            else
            {
                this.setFontButton.Visible = false;
                this.setFontButton.Enabled = false;
                this.setColorButton.Enabled = false;
                this.setColorButton.Width = this.Width - 6;
            }
        }

        private void OnRetrieveItem( object sender, RetrieveVirtualItemEventArgs e )
        {
            TextColor color = Program.GameMode.Colors[e.ItemIndex];
            ListViewItem item = new ListViewItem();
            if( color.Color != Color.Empty )
                item.ForeColor = color.Color;
            else
            {
                TextColor namedColor = Program.GameMode.Colors[color.ColorName];
                item.ForeColor = namedColor.Color;
            }
            item.Font = new Font( "Tahoma", 10, FontStyle.Bold, GraphicsUnit.Point );
            item.Text = color.Name;
            item.SubItems.Add( color.ColorName );
            e.Item = item;
        }

        private void OnBackgroundColorChanged( object sender, EventArgs e )
        {
            this.colorList.Invalidate();
        }

        private void OnColorsChanged( object sender, ListChangedEventArgs e )
        {
            if( !Program.GameMode.Colors.Loaded )
                return;
            
            switch( e.ListChangedType )
            {
                case ListChangedType.Reset:
                case ListChangedType.ItemAdded:
                case ListChangedType.ItemDeleted:
                    this.colorList.VirtualListSize = Program.GameMode.Colors.Count;
                    break;
                case ListChangedType.ItemChanged:
                    if( e.NewIndex > 0 && e.NewIndex < this.colorList.VirtualListSize )
                        this.colorList.RedrawItems( e.NewIndex, e.NewIndex, false );
                    break;
                default:
                    break;
            }
        }

        private void OnColorsLoaded( object sender, EventArgs e )
        {
            this.colorList.VirtualListSize = Program.GameMode.Colors.Count;
        }

        private void OnColorActivated( object sender, EventArgs e )
        {
            if( this.colorList.SelectedIndices.Count != 1 )
                return;

            int index = this.colorList.SelectedIndices[0];
            TextColor color = Program.GameMode.Colors[index];
            colorDialog.Color = color.Color;
            colorDialog.CustomColors = Program.Settings.CustomColors;
            if( colorDialog.ShowDialog() == DialogResult.OK )
            {
                color.Color = colorDialog.Color;
            }
            Program.Settings.CustomColors = colorDialog.CustomColors;
        }

        private void OnFontActivated( object sender, EventArgs e )
        {
            if( this.colorList.SelectedIndices.Count != 1 )
                return;

            int index = this.colorList.SelectedIndices[0];
            TextFont font = (Program.GameMode.Colors[index] as TextFont);
            if( font != null )
            {
                using( FontDialog dialog = new FontDialog() )
                {
                    dialog.FontFamily = font.FontFamily;
                    dialog.FontStyle = font.FontStyle;
                    dialog.FontSize = font.FontSize;
                    dialog.Speed = font.Speed;
                    dialog.WaitOnScreen = font.WaitOnScreen;
                    dialog.Direction = font.Direction;
                    if( dialog.ShowDialog() == DialogResult.OK )
                    {
                        font.FontFamily = dialog.FontFamily;
                        font.FontStyle = dialog.FontStyle;
                        font.FontSize = dialog.FontSize;
                        font.Speed = dialog.Speed;
                        font.WaitOnScreen = dialog.WaitOnScreen;
                        font.Direction = dialog.Direction;
                    }
                }
            }
        }

        private void OnItemSelected( object sender, EventArgs e )
        {
            if( this.colorList.SelectedIndices.Count != 1 )
            {
                this.setFontButton.Enabled = false;
                this.setColorButton.Enabled = false;
            }
            else
            {
                int index = this.colorList.SelectedIndices[0];
                TextFont font = (Program.GameMode.Colors[index] as TextFont);
                if( font != null )
                    this.setFontButton.Enabled = true;
                else
                    this.setFontButton.Enabled = false;
                this.setColorButton.Enabled = true;
            }
        }
    }
}
