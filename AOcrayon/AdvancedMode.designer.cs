﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOcrayon
{
    partial class AdvancedMode
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        /// <summary>
        /// Disposes resources used by the control.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        
        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.setColorButton = new System.Windows.Forms.Button();
            this.advancedHelpLabel = new Twp.Controls.HtmlRenderer.HtmlLabel();
            this.setFontButton = new System.Windows.Forms.Button();
            this.colorList = new AOcrayon.ColorListView();
            this.SuspendLayout();
            //
            // colorDialog
            //
            this.colorDialog.AnyColor = true;
            this.colorDialog.FullOpen = true;
            //
            // setColorButton
            //
            this.setColorButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.setColorButton.Location = new System.Drawing.Point(3, 229);
            this.setColorButton.Name = "setColorButton";
            this.setColorButton.Size = new System.Drawing.Size(389, 24);
            this.setColorButton.TabIndex = 1;
            this.setColorButton.Text = "Change selected color";
            this.setColorButton.UseVisualStyleBackColor = true;
            this.setColorButton.Click += new System.EventHandler(this.OnColorActivated);
            //
            // advancedHelpLabel
            //
            this.advancedHelpLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.advancedHelpLabel.AutoSize = false;
            this.advancedHelpLabel.BackColor = System.Drawing.SystemColors.Window;
            this.advancedHelpLabel.BaseStylesheet = null;
            this.advancedHelpLabel.Location = new System.Drawing.Point(0, 256);
            this.advancedHelpLabel.Name = "advancedHelpLabel";
            this.advancedHelpLabel.Size = new System.Drawing.Size(395, 140);
            this.advancedHelpLabel.TabIndex = 3;
            //
            // setFontButton
            //
            this.setFontButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.setFontButton.Location = new System.Drawing.Point(184, 229);
            this.setFontButton.Name = "setFontButton";
            this.setFontButton.Size = new System.Drawing.Size(208, 24);
            this.setFontButton.TabIndex = 2;
            this.setFontButton.Text = "Change selected font";
            this.setFontButton.UseVisualStyleBackColor = true;
            this.setFontButton.Visible = false;
            this.setFontButton.Click += new System.EventHandler(this.OnFontActivated);
            //
            // colorList
            //
            this.colorList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                    | System.Windows.Forms.AnchorStyles.Left)
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.colorList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colorList.FullRowSelect = true;
            this.colorList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.colorList.Location = new System.Drawing.Point(3, 3);
            this.colorList.MultiSelect = false;
            this.colorList.Name = "colorList";
            this.colorList.OwnerDraw = true;
            this.colorList.ShowGroups = false;
            this.colorList.Size = new System.Drawing.Size(389, 220);
            this.colorList.TabIndex = 0;
            this.colorList.UseCompatibleStateImageBehavior = false;
            this.colorList.View = System.Windows.Forms.View.Details;
            this.colorList.VirtualMode = true;
            this.colorList.ItemActivate += new System.EventHandler(this.OnColorActivated);
            this.colorList.RetrieveVirtualItem += new System.Windows.Forms.RetrieveVirtualItemEventHandler(this.OnRetrieveItem);
            this.colorList.SelectedIndexChanged += new System.EventHandler(this.OnItemSelected);
            //
            // AdvancedMode
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.setFontButton);
            this.Controls.Add(this.colorList);
            this.Controls.Add(this.setColorButton);
            this.Controls.Add(this.advancedHelpLabel);
            this.Name = "AdvancedMode";
            this.Size = new System.Drawing.Size(395, 396);
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.ColorDialog colorDialog;
        private AOcrayon.ColorListView colorList;
        private System.Windows.Forms.Button setColorButton;
        private Twp.Controls.HtmlRenderer.HtmlLabel advancedHelpLabel;
        private System.Windows.Forms.Button setFontButton;
    }
}
