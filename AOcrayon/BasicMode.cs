﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014s
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Windows.Forms;
using System.Windows.Forms.Layout;

using FCGame;
using Twp.Controls;

namespace AOcrayon
{
    /// <summary>
    /// Description of BasicMode.
    /// </summary>
    public partial class BasicMode : UserControl
    {
        public BasicMode()
        {
            InitializeComponent();
            this.basicHelpLabel.BaseStylesheet  = Properties.Resources.BaseCSS;
        }

        private VerticalLayout layoutEngine;
        public override LayoutEngine LayoutEngine
        {
            get
            {
                if( this.layoutEngine == null )
                {
                    this.layoutEngine = new VerticalLayout();
                    this.layoutEngine.HorizontalAlign = HorizontalLayoutAlignment.Stretch;
                    this.layoutEngine.StretchLast = true;
                }
                return this.layoutEngine;
            }
        }

        public void Init()
        {
            int height = 0;
            this.SuspendLayout();

            foreach( Category cat in Program.GameMode.Categories.Items )
            {
                BasicModePage page = new BasicModePage( cat );
                this.tabControl.TabPages.Add( page );

                if( page.LayoutHeight > height )
                    height = page.LayoutHeight;
            }

            this.tabControl.Height = height + this.tabControl.ItemSize.Height + this.tabControl.Margin.Vertical;

            string basicHelp = Program.GameMode.Resources.GetString( "BasicHelp", Application.CurrentCulture );
            if( String.IsNullOrEmpty( basicHelp ) )
                this.basicHelpLabel.Visible = false;
            else
                this.basicHelpLabel.Text = basicHelp;

            this.ResumeLayout( true );
            this.PerformLayout();
        }
    }
}
