﻿using System;
using System.Windows.Forms;
using System.Windows.Forms.Layout;
using System.Drawing;

namespace AOcrayon
{
    public class BasicModeLayout : LayoutEngine
    {
        private int height;
        public int Height
        {
            get { return this.height; }
        }

        public override bool Layout( object container, System.Windows.Forms.LayoutEventArgs layoutEventArgs )
        {
            Control parent = container as Control;

            Rectangle pDisplayRect = parent.DisplayRectangle;
            Point nextLocation = pDisplayRect.Location;
            nextLocation.Offset( parent.Margin.Left, parent.Margin.Top );

            foreach( Control c in parent.Controls )
            {
                //Twp.Utilities.Log.Debug( "[BasicModeLayout.Layout] nextLocation: {0}", nextLocation );
                if( !c.Visible )
                    continue;

                c.Location = nextLocation;
                c.Width = pDisplayRect.Width - parent.Margin.Horizontal;

                nextLocation.Offset( 0, c.Height + c.Margin.Bottom );
            }

            this.height = nextLocation.Y + parent.Margin.Bottom;
            //Twp.Utilities.Log.Debug( "[BasicModeLayout.Layout] height: {0}", this.height );

            return true;
        }
    }
}
