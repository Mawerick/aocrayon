﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

using FCGame;
using Twp.Utilities;

namespace AOcrayon
{
    /// <summary>
    /// Description of ColorControl.
    /// </summary>
    [DefaultMember( "Text" )]
    public partial class ColorControl : UserControl
    {
        private ColorGroup colorGroup;
        
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Button[] colorButtons;
        private System.Windows.Forms.Button fontButton;
        
        [Category( "Appearance" )]
        [Description( "Defines the width of the area used by the buttons." )]
        [DefaultValue( 120 )]
        public int ButtonAreaWidth
        {
            get { return this.buttonAreaWidth; }
            set
            {
                if( this.buttonAreaWidth != value )
                {
                    this.buttonAreaWidth = value;
                    this.ResizeButtons();
                }
            }
        }
        private int buttonAreaWidth = 120;

        protected override Padding DefaultMargin
        {
            get { return new Padding( 2 ); }
        }

        //private HorizontalLayout layoutEngine;
        //public override LayoutEngine LayoutEngine
        //{
        //    get
        //    {
        //        if( this.layoutEngine == null )
        //            this.layoutEngine = new HorizontalLayout();
        //        return this.layoutEngine;
        //    }
        //}

        public ColorControl( ColorGroup colorGroup )
        {
            if( colorGroup == null )
                throw new ArgumentNullException( "colorGroup" );
            
            this.colorGroup = colorGroup;
            
            InitializeComponent();
            
            Program.Settings.BackgroundColorChanged += new EventHandler( OnUpdateColor );
            Program.GameMode.Colors.ListChanged += new ListChangedEventHandler( OnColorsChanged );
            Program.GameMode.Colors.LoadFinished += new EventHandler(OnColorsLoaded);
        }
        
        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            
            //
            // label
            //
            this.label = new Label();
            this.label.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;
            this.label.BorderStyle = BorderStyle.Fixed3D;
            this.label.Font = new Font( "Tahoma", 9.75F, FontStyle.Bold, GraphicsUnit.Point, (byte) 0 );
            this.label.Location = new Point( 0, 0 );
            this.label.Name = "label";
            this.label.Padding = new Padding( 2, 0, 0, 0 );
            this.label.Size = new Size( 237, 25 );
            this.label.TabIndex = 0;
            this.label.Text = this.colorGroup.Text;
            this.label.TextAlign = ContentAlignment.MiddleLeft;
            this.Controls.Add( this.label );

            //
            // colorButtons
            //
            if( this.colorGroup is NamedColorGroup )
            {
                NamedColorGroup ncg = (this.colorGroup as NamedColorGroup);
                this.colorButtons = new Button[ncg.Names.Length];
                
                for( int i = 0; i < ncg.Names.Length; i++ )
                {
                    this.colorButtons[i] = new Button();
                    this.colorButtons[i].Anchor = AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;
                    this.colorButtons[i].Location = new Point( 240, 0 );
                    this.colorButtons[i].Name = "colorButton" + i.ToString();
                    this.colorButtons[i].Size = new Size( this.buttonAreaWidth, 25 );
                    this.colorButtons[i].TabIndex = 1 + i;
                    this.colorButtons[i].Tag = i;
                    this.colorButtons[i].UseVisualStyleBackColor = true;
                    this.colorButtons[i].Click += new EventHandler( this.OnColorButtonClicked );
                    this.Controls.Add( this.colorButtons[i] );
                }
            }
            else
            {
                this.colorButtons = new Button[] { new Button() };
                this.colorButtons[0].Anchor = AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;
                this.colorButtons[0].Location = new Point( 240, 0 );
                this.colorButtons[0].Name = "colorButton";
                this.colorButtons[0].Size = new Size( this.buttonAreaWidth, 25 );
                this.colorButtons[0].TabIndex = 1;
                this.colorButtons[0].Tag = 0;
                this.colorButtons[0].UseVisualStyleBackColor = true;
                this.colorButtons[0].Click += new EventHandler( this.OnColorButtonClicked );
                   this.Controls.Add( this.colorButtons[0] );

                if( this.colorGroup is FontColorGroup )
                {
                    //
                    // fontButton
                    //
                    this.fontButton = new Button();
                    this.fontButton.Anchor = AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;
                    this.fontButton.Location = new Point( 335, 0 );
                    this.fontButton.Name = "fontButton";
                    this.fontButton.Size = new Size( 25, 25 );
                    this.fontButton.TabIndex = 2;
                    this.fontButton.Text = "F";
                    this.fontButton.UseVisualStyleBackColor = true;
                    this.fontButton.Click += new EventHandler( this.OnFontButtonClicked );
                    this.Controls.Add( this.fontButton );
                }
            }
            this.ResizeButtons();
            this.UpdateColor();
            
            //
            // ColorControl
            //
            this.AutoScaleDimensions = new SizeF( 6F, 13F );
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Name = "ColorControl";
            this.Size = new System.Drawing.Size( 360, 25 );
            this.ResumeLayout( false );
        }

        private void ResizeButtons()
        {
            this.SuspendLayout();

            int width = this.buttonAreaWidth;
            if( this.fontButton != null )
            {
                width -= 25;
            }
            width /= this.colorButtons.Length;
            
            for( int i = 0; i < this.colorButtons.Length; i++ )
            {
                this.colorButtons[i].Location = new Point( 240 + (width * i), 0 );
                this.colorButtons[i].Size = new Size( width - 2, 25 );
            }
            
            this.ResumeLayout( false );
        }
        
        private TextColor GetColor( int index )
        {
            TextColor color = Program.GameMode.Colors[this.colorGroup.Colors[index]];
            if( color == null )
            {
                // error... color doesn't exist...
                return null;
            }
            else if( color.Color == Color.Empty )
            {
                TextColor namedColor = Program.GameMode.Colors[color.ColorName];
                if( namedColor != null )
                {
                    color.Color = namedColor.Color;
                }
            }
            return color;
        }
        
        public void UpdateColor()
        {
            if( this.colorGroup.Colors.Count <= 0 )
                return;

            TextColor labelColor;
            if( this.colorGroup is NamedColorGroup )
                labelColor = this.GetColor( 1 );
            else
                labelColor = this.GetColor( 0 );

            if( labelColor != null )
                this.label.ForeColor = labelColor.Color;
            else
                this.label.ForeColor = Color.Red;
            
            this.label.BackColor = Program.Settings.BackgroundColor;

            for( int i = 0; i < this.colorButtons.Length; i++ )
            {
                try
                {
                    TextColor color = this.GetColor( i );
                    if( color == null )
                    {
                        this.colorButtons[i].ForeColor = Color.Red;
                        this.colorButtons[i].BackColor = Color.Empty;
                        this.colorButtons[i].Text = "Missing!";
                    }
                    else
                    {
                        this.colorButtons[i].BackColor = color.Color;
                        if( this.colorGroup is NamedColorGroup )
                        {
                            NamedColorGroup ncg = (this.colorGroup as NamedColorGroup );
                            this.colorButtons[i].Text = ncg.Names[i];
                        }
                        else
                        {
                            this.colorButtons[i].Text = null;
                        }
                    }
                }
                catch( Exception ex )
                {
                    Log.Debug( "[ColorControl.UpdateColor] Exception: {0}", ex );
                    throw;
                }
            }
        }

        internal void OnUpdateColor( object sender, EventArgs e )
        {
            this.label.BackColor = Program.Settings.BackgroundColor;
        }

        internal void OnColorsChanged( object sender, ListChangedEventArgs e )
        {
            if( !Program.GameMode.Colors.Loaded )
                return;

            switch( e.ListChangedType )
            {
                case ListChangedType.ItemAdded:
                case ListChangedType.ItemChanged:
                    if( this.colorGroup.Colors.Contains( Program.GameMode.Colors[e.NewIndex].Name ) )
                    {
                        this.UpdateColor();
                    }
                    break;
                case ListChangedType.Reset:
                    this.UpdateColor();
                    break;
            }
        }
        
        private void OnColorsLoaded( object sender, EventArgs e )
        {
            this.UpdateColor();
        }

        private bool ColorGroupsExists()
        {
            foreach( string group in this.colorGroup.Colors )
            {
                if(  Program.GameMode.Colors[group] == null )
                    return false;
            }
            return true;
        }
        
        private bool VerifyColorGroups()
        {
            if( this.colorGroup.Colors.Count <= 0 )
            {
                Log.Error( Properties.Resources.ColorGroupsMissing, this.Text );
                return false;
            }
            
            if( this.ColorGroupsExists() )
                return true;
            
            DialogResult dr = MessageBox.Show( String.Format( Properties.Resources.ColorMissing, this.Text ),
                                               Properties.Resources.ColorMissingCaption,
                                               MessageBoxButtons.YesNo, MessageBoxIcon.Question );
            if( dr == DialogResult.Yes )
            {
                foreach( string group in this.colorGroup.Colors )
                {
                    if( Program.GameMode.Colors[group] == null )
                    {
                        Program.GameMode.Colors.Add( group );
                    }
                }
                if( !this.ColorGroupsExists() )
                {
                    Log.Error( Properties.Resources.ColorAddFailed, this.Text );
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        private void OnColorButtonClicked( object sender, EventArgs e )
        {
            Button button = (sender as Button);
            if( button == null )
                return;
            
            if( this.VerifyColorGroups() == false )
                return;
            
            int index = (int) button.Tag;
            
            TextColor color = Program.GameMode.Colors[this.colorGroup.Colors[index]];
            if( color.Color == Color.Empty )
            {
                color = Program.GameMode.Colors[color.ColorName];
            }

            using( ColorDialog dialog = new ColorDialog() )
            {
                dialog.AnyColor = true;
                dialog.FullOpen = true;
                dialog.Color = color.Color;
                dialog.CustomColors = Program.Settings.CustomColors;
                if( dialog.ShowDialog() == DialogResult.OK )
                {
                    if( this.colorGroup is NamedColorGroup )
                    {
                        color.Color = dialog.Color;
                    }
                    else
                    {
                        foreach( string group in this.colorGroup.Colors )
                        {
                            color = Program.GameMode.Colors[group];
                            if( color != null )
                                color.Color = dialog.Color;
                        }
                    }
                    this.UpdateColor();
                }
                Program.Settings.CustomColors = dialog.CustomColors;
            }
        }

        private void OnFontButtonClicked( object sender, EventArgs e )
        {
            if( this.colorGroup.Colors.Count <= 0 )
            {
                Log.Error( Properties.Resources.ColorGroupsMissing, this.Text );
                return;
            }

            TextFont font = (Program.GameMode.Colors[this.colorGroup.Colors[0]] as TextFont);
            if( font == null )
            {
                DialogResult dr = MessageBox.Show( String.Format( Properties.Resources.FontMissing, this.Text ),
                                                   Properties.Resources.FontMissingCaption,
                                                   MessageBoxButtons.YesNo, MessageBoxIcon.Question );
                if( dr == DialogResult.Yes )
                {
                    foreach( string group in this.colorGroup.Colors )
                    {
                        if( Program.GameMode.Colors[group] == null )
                        {
                            Program.GameMode.Colors.Add( group );
                        }
                    }
                    font = (Program.GameMode.Colors[this.colorGroup.Colors[0]] as TextFont);
                    if( font == null )
                    {
                        Log.Error( Properties.Resources.FontAddFailed, this.Text );
                        return;
                    }
                }
                else
                {
                    return;
                }
            }

            using( FontDialog dialog = new FontDialog() )
            {
                dialog.FontFamily = font.FontFamily;
                dialog.FontStyle = font.FontStyle;
                dialog.FontSize = font.FontSize;
                dialog.Speed = font.Speed;
                dialog.WaitOnScreen = font.WaitOnScreen;
                dialog.Direction = font.Direction;
                if( dialog.ShowDialog() == DialogResult.OK )
                {
                    foreach( string group in this.colorGroup.Colors )
                    {
                        font = (Program.GameMode.Colors[group] as TextFont);
                        if( font != null )
                        {
                            font.FontFamily = dialog.FontFamily;
                            font.FontStyle = dialog.FontStyle;
                            font.FontSize = dialog.FontSize;
                            font.Speed = dialog.Speed;
                            font.WaitOnScreen = dialog.WaitOnScreen;
                            font.Direction = dialog.Direction;
                        }
                    }
                }
            }
        }
    }
}
