﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.Drawing;
using System.Windows.Forms;
using Twp.Controls;

namespace AOcrayon
{
    /// <summary>
    /// Description of ColorListView.
    /// </summary>
    public class ColorListView : FillListView
    {
        public ColorListView()
        {
            this.Columns.Add( "Color group", 210 );
            this.Columns.Add( "Color", 125 );
            this.Font = new Font( "Microsoft Sans Serif", 9.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte) (0)) );
            this.FullRowSelect = true;
            this.HeaderStyle = ColumnHeaderStyle.Nonclickable;
            this.MultiSelect = false;
            this.OwnerDraw = true;
            this.ShowGroups = false;
            this.StretchIndex = 0;
            this.View = View.Details;
            this.DoubleBuffered = true;
        }

        protected override void OnDrawColumnHeader( DrawListViewColumnHeaderEventArgs e )
        {
            e.DrawBackground();
            using( Font font = new Font( e.Font.FontFamily, 8 ) )
            {
                Rectangle rect = e.Bounds;
                rect.Inflate( -3, 0 );
                StringFormat sf = new StringFormat();
                sf.Alignment = StringAlignment.Near;
                sf.LineAlignment = StringAlignment.Center;
                sf.Trimming = StringTrimming.EllipsisCharacter;
                e.Graphics.DrawString( e.Header.Text, font, new SolidBrush( e.ForeColor ), rect, sf );
            }
        }

        protected override void OnDrawSubItem( DrawListViewSubItemEventArgs e )
        {
            using( Brush brush = new SolidBrush( Program.Settings.BackgroundColor ) )
            {
                e.Graphics.FillRectangle( brush, e.Bounds );
            }
            Rectangle bounds = e.Bounds;
            bounds.Height -= 1;
            bounds.Width -= 1;
            
            if( e.Item.Selected )
            {
                e.Graphics.DrawLine( Pens.OrangeRed, bounds.Left, bounds.Top, bounds.Right, bounds.Top );
                e.Graphics.DrawLine( Pens.OrangeRed, bounds.Left, bounds.Bottom, bounds.Right, bounds.Bottom );
                if( e.ColumnIndex == 0 )
                    e.Graphics.DrawLine( Pens.OrangeRed, bounds.Left, bounds.Top, bounds.Left, bounds.Bottom );
                else
                    e.Graphics.DrawLine( Pens.OrangeRed, bounds.Right, bounds.Top, bounds.Right, bounds.Bottom );
            }

            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Near;
            sf.LineAlignment = StringAlignment.Center;
            sf.Trimming = StringTrimming.EllipsisCharacter;
            if( e.ColumnIndex == 0 )
            {
                bounds.Inflate( -3, 0 );
                e.Graphics.DrawString( e.SubItem.Text, e.Item.Font, new SolidBrush( e.Item.ForeColor ), bounds, sf );
            }
            else
            {
                Rectangle rect = new Rectangle( bounds.Location, new Size( 26, bounds.Height - 5 ) );
                rect.Offset( 3, 3 );
                e.Graphics.FillRectangle( new SolidBrush( e.Item.ForeColor ), rect );

                bounds.Inflate( -13, 0 );
                bounds.Offset( 16, 0 );
                e.Graphics.DrawString( e.SubItem.Text, e.Item.Font, new SolidBrush( e.Item.ForeColor ), bounds, sf );
            }
        }
    }
}
