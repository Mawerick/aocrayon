﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOcrayon
{
    partial class FontDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fontBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.styleBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.sizeBox = new System.Windows.Forms.ComboBox();
            this.line1 = new Twp.Controls.Line();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.speedSlider = new Twp.Controls.Slider();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.waitSlider = new Twp.Controls.Slider();
            this.line2 = new Twp.Controls.Line();
            this.label6 = new System.Windows.Forms.Label();
            this.leftButton = new System.Windows.Forms.RadioButton();
            this.centerButton = new System.Windows.Forms.RadioButton();
            this.rightButton = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            //
            // fontBox
            //
            this.fontBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fontBox.FormattingEnabled = true;
            this.fontBox.Items.AddRange(new object[] {
            "hyborian",
            "hyborian3",
            "SapphIIM",
            "YDVCwritM"});
            this.fontBox.Location = new System.Drawing.Point(12, 25);
            this.fontBox.Name = "fontBox";
            this.fontBox.Size = new System.Drawing.Size(160, 21);
            this.fontBox.TabIndex = 1;
            //
            // label1
            //
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "&Font:";
            //
            // styleBox
            //
            this.styleBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.styleBox.FormattingEnabled = true;
            this.styleBox.Items.AddRange(new object[] {
            "regular",
            "bold",
            "italic"});
            this.styleBox.Location = new System.Drawing.Point(12, 65);
            this.styleBox.Name = "styleBox";
            this.styleBox.Size = new System.Drawing.Size(160, 21);
            this.styleBox.TabIndex = 3;
            //
            // label2
            //
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "&Style:";
            //
            // label3
            //
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "S&ize:";
            //
            // sizeBox
            //
            this.sizeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sizeBox.FormattingEnabled = true;
            this.sizeBox.Items.AddRange(new object[] {
            "small",
            "medium",
            "large"});
            this.sizeBox.Location = new System.Drawing.Point(12, 105);
            this.sizeBox.Name = "sizeBox";
            this.sizeBox.Size = new System.Drawing.Size(160, 21);
            this.sizeBox.TabIndex = 5;
            //
            // line1
            //
            this.line1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.line1.Location = new System.Drawing.Point(0, 136);
            this.line1.Name = "line1";
            this.line1.Size = new System.Drawing.Size(372, 10);
            //
            // cancelButton
            //
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(285, 152);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 15;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            //
            // okButton
            //
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(204, 152);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 14;
            this.okButton.Text = "&Ok";
            this.okButton.UseVisualStyleBackColor = true;
            //
            // speedSlider
            //
            this.speedSlider.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.speedSlider.Location = new System.Drawing.Point(200, 25);
            this.speedSlider.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.speedSlider.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.speedSlider.Name = "speedSlider";
            this.speedSlider.PageStep = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.speedSlider.Size = new System.Drawing.Size(160, 21);
            this.speedSlider.Step = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.speedSlider.TabIndex = 7;
            this.speedSlider.TickFrequency = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.speedSlider.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            //
            // label4
            //
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(200, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "S&peed:";
            //
            // label5
            //
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(200, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "&Wait on screen:";
            //
            // waitSlider
            //
            this.waitSlider.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.waitSlider.DecimalPlaces = 1;
            this.waitSlider.Location = new System.Drawing.Point(200, 65);
            this.waitSlider.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.waitSlider.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.waitSlider.Name = "waitSlider";
            this.waitSlider.PageStep = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.waitSlider.Size = new System.Drawing.Size(160, 21);
            this.waitSlider.Step = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.waitSlider.TabIndex = 9;
            this.waitSlider.TickFrequency = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.waitSlider.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            //
            // line2
            //
            this.line2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.line2.Location = new System.Drawing.Point(182, 0);
            this.line2.Name = "line2";
            this.line2.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.line2.Size = new System.Drawing.Size(10, 140);
            //
            // label6
            //
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(200, 89);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "&Direction:";
            //
            // leftButton
            //
            this.leftButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.leftButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.leftButton.Location = new System.Drawing.Point(200, 105);
            this.leftButton.Name = "leftButton";
            this.leftButton.Size = new System.Drawing.Size(52, 21);
            this.leftButton.TabIndex = 11;
            this.leftButton.TabStop = true;
            this.leftButton.Text = "Left";
            this.leftButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.leftButton.UseVisualStyleBackColor = true;
            //
            // centerButton
            //
            this.centerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.centerButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.centerButton.Checked = true;
            this.centerButton.Location = new System.Drawing.Point(254, 105);
            this.centerButton.Name = "centerButton";
            this.centerButton.Size = new System.Drawing.Size(52, 21);
            this.centerButton.TabIndex = 12;
            this.centerButton.TabStop = true;
            this.centerButton.Text = "Center";
            this.centerButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.centerButton.UseVisualStyleBackColor = true;
            //
            // rightButton
            //
            this.rightButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rightButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.rightButton.Location = new System.Drawing.Point(308, 105);
            this.rightButton.Name = "rightButton";
            this.rightButton.Size = new System.Drawing.Size(52, 21);
            this.rightButton.TabIndex = 13;
            this.rightButton.TabStop = true;
            this.rightButton.Text = "Right";
            this.rightButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rightButton.UseVisualStyleBackColor = true;
            //
            // FontDialog
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 187);
            this.Controls.Add(this.rightButton);
            this.Controls.Add(this.centerButton);
            this.Controls.Add(this.leftButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.line2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.waitSlider);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.speedSlider);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.line1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.sizeBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.styleBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.fontBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FontDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Font";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox fontBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox styleBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox sizeBox;
        private Twp.Controls.Line line1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private Twp.Controls.Slider speedSlider;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Twp.Controls.Slider waitSlider;
        private Twp.Controls.Line line2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton leftButton;
        private System.Windows.Forms.RadioButton centerButton;
        private System.Windows.Forms.RadioButton rightButton;
    }
}