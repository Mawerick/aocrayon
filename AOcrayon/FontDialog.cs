﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Windows.Forms;

namespace AOcrayon
{
    public partial class FontDialog : Form
    {
        public FontDialog()
        {
            InitializeComponent();
        }

        public string FontFamily
        {
            get { return (string) this.fontBox.SelectedItem; }
            set { this.fontBox.SelectedIndex = this.fontBox.FindStringExact( value ); }
        }

        public string FontStyle
        {
            get { return (string) this.styleBox.SelectedItem; }
            set { this.styleBox.SelectedIndex = this.styleBox.FindStringExact( value ); }
        }

        public string FontSize
        {
            get { return (string) this.sizeBox.SelectedItem; }
            set { this.sizeBox.SelectedIndex = this.sizeBox.FindStringExact( value ); }
        }

        public int Speed
        {
            get { return Decimal.ToInt32( this.speedSlider.Value ); }
            set { this.speedSlider.Value = value; }
        }

        public decimal WaitOnScreen
        {
            get { return this.waitSlider.Value; }
            set { this.waitSlider.Value = value; }
        }

        public int Direction
        {
            get
            {
                if( this.leftButton.Checked )
                    return -1;
                else if( this.rightButton.Checked )
                    return 1;
                else
                    return 0;
            }
            set
            {
                if( value == -1 )
                    this.leftButton.Checked = true;
                else if( value == 1 )
                    this.rightButton.Checked = true;
                else
                    this.centerButton.Checked = true;
            }
        }
    }
}
