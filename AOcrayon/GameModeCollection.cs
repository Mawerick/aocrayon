﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using FCGame;

namespace AOcrayon
{
    public class GameModeCollection : CollectionBase
    {
        public void Add( IGameMode gameMode )
        {
            this.List.Add( gameMode );
        }

        public void Remove( IGameMode gameMode )
        {
            this.List.Remove( gameMode );
        }

        public IGameMode this[int index]
        {
            get { return (IGameMode) this.List[index]; }
        }

        public IGameMode First()
        {
            if( this.List.Count < 1 )
                return null;

            return (IGameMode) this.List[0];
        }

        public IGameMode Find( string gameId )
        {
            if( String.IsNullOrEmpty( gameId ) )
                return null;

            IGameMode gameMode = null;

            foreach( IGameMode gm in this.List )
            {
                if( gm.GameID.Equals( gameId ) )
                {
                    gameMode = gm;
                    break;
                }
            }

            return gameMode;
        }
    }
}
