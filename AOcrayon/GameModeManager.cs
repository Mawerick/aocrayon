﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using System.Reflection;

using FCGame;
using Twp.Utilities;

namespace AOcrayon
{
    public class GameModeManager //: IGameModeHost
    {
        public GameModeManager()
        {
        }

        private GameModeCollection modes = new GameModeCollection();
        public GameModeCollection Modes
        {
            get { return this.modes; }
            set { this.modes = value; }
        }

        public void FindModes()
        {
            this.FindModes( AppDomain.CurrentDomain.BaseDirectory );
        }

        public void FindModes( string path )
        {
            this.modes.Clear();

            foreach( string fileName in Directory.GetFiles( path, "FCGame.*.dll" ) )
            {
                this.AddMode( fileName );
            }
        }

        public void CloseGameModes()
        {
            foreach( IGameMode mode in this.modes )
            {
                mode.Dispose();
            }
            this.modes.Clear();
        }

        public void AddMode( string fileName )
        {
            Assembly asm = Assembly.LoadFrom( fileName );
            foreach( Type type in asm.GetTypes() )
            {
                if( type.IsPublic && !type.IsAbstract )
                {
                    Type gmInterface = type.GetInterface( "FCGame.IGameMode", true );
                    if( gmInterface != null )
                    {
                        IGameMode mode = (IGameMode) Activator.CreateInstance( asm.GetType( type.ToString() ) );
                        Log.Debug( "[GameModeManager.AddMode] Mode: {0}", mode.Name );
                        this.modes.Add( mode );
                    }
                }
            }
        }
    }
}
