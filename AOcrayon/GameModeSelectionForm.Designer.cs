﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOcrayon
{
    partial class GameModeSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label = new System.Windows.Forms.Label();
            this.line = new Twp.Controls.Line();
            this.rememberModeBox = new System.Windows.Forms.CheckBox();
            this.okButton = new System.Windows.Forms.Button();
            this.gameModeList = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            //
            // label
            //
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(12, 9);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(184, 13);
            this.label.TabIndex = 0;
            this.label.Text = "Select the game you wish to manage:";
            //
            // line
            //
            this.line.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.line.Location = new System.Drawing.Point(0, 236);
            this.line.Name = "line";
            this.line.Size = new System.Drawing.Size(254, 10);
            //
            // rememberModeBox
            //
            this.rememberModeBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rememberModeBox.AutoSize = true;
            this.rememberModeBox.Location = new System.Drawing.Point(15, 256);
            this.rememberModeBox.Name = "rememberModeBox";
            this.rememberModeBox.Size = new System.Drawing.Size(100, 17);
            this.rememberModeBox.TabIndex = 2;
            this.rememberModeBox.Text = "Don\'t ask again";
            this.rememberModeBox.UseVisualStyleBackColor = true;
            //
            // okButton
            //
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(168, 252);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 3;
            this.okButton.Text = "&Ok";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.OnOkClicked);
            //
            // gameModeList
            //
            this.gameModeList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.gameModeList.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.gameModeList.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameModeList.FormattingEnabled = true;
            this.gameModeList.ItemHeight = 66;
            this.gameModeList.Location = new System.Drawing.Point(12, 25);
            this.gameModeList.Name = "gameModeList";
            this.gameModeList.Size = new System.Drawing.Size(230, 202);
            this.gameModeList.TabIndex = 4;
            this.gameModeList.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawItem);
            this.gameModeList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.OnDoubleClick);
            //
            // GameModeSelectionForm
            //
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(254, 284);
            this.Controls.Add(this.gameModeList);
            this.Controls.Add(this.label);
            this.Controls.Add(this.line);
            this.Controls.Add(this.rememberModeBox);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "GameModeSelectionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AOcrayon";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label label;
        private Twp.Controls.Line line;
        private System.Windows.Forms.CheckBox rememberModeBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.ListBox gameModeList;
    }
}