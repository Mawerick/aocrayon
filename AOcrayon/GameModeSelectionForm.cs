﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Windows.Forms;

using FCGame;

namespace AOcrayon
{
    public partial class GameModeSelectionForm : Form
    {
        public GameModeSelectionForm()
        {
            InitializeComponent();

            this.gameModeList.DataSource = Program.GameModes.Modes;
            this.gameModeList.DisplayMember = "Name";
            this.gameModeList.ValueMember = "GameID";
            this.gameModeList.SelectedItem = Program.GameModes.Modes.Find( Program.Settings.LastGameMode );

            this.rememberModeBox.Checked = Program.Settings.RememberMode;
        }

        private void OnOkClicked( object sender, EventArgs e )
        {
            Program.Settings.LastGameMode = (this.gameModeList.SelectedItem as IGameMode).GameID;
            Program.Settings.RememberMode = this.rememberModeBox.Checked;
        }

        // Draws GameMode items in list, with icon.
        private void OnDrawItem( object sender, DrawItemEventArgs e )
        {
            e.DrawBackground();
            Rectangle bounds = e.Bounds;
            IGameMode gm = Program.GameModes.Modes[e.Index];
            Rectangle imgRect = new Rectangle( bounds.Location, new Size( 64, 64 ) );
            imgRect.Offset( 1, 1 );
            e.Graphics.DrawImage( (Image) gm.Resources.GetObject( "Icon" ), imgRect );
            bounds.Offset( 68, 0 );
            bounds.Width -= 68;
            TextFormatFlags tff = TextFormatFlags.EndEllipsis | TextFormatFlags.Left | TextFormatFlags.VerticalCenter;
            TextRenderer.DrawText( e.Graphics, gm.Name, this.gameModeList.Font, bounds, this.gameModeList.ForeColor, tff );
        }
        
        private void OnDoubleClick( object sender, MouseEventArgs e )
        {
            int index = this.gameModeList.IndexFromPoint( e.Location );
            if( index != ListBox.NoMatches )
            {
                if( this.gameModeList.SelectedIndex != index )
                    this.gameModeList.SelectedIndex = index;
                
                this.okButton.PerformClick();
            }
        }
    }
}
