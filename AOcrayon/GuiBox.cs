﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Windows.Forms;
using Twp.Utilities;

namespace AOcrayon
{
    public partial class GuiBox : UserControl
    {
        public GuiBox()
        {
            InitializeComponent();
            this.button.Image = Properties.Resources.Button_Refresh;
        }

        private bool loaded = false;

        private void OnLoad( object sender, EventArgs e )
        {
            if( this.DesignMode )
                return;

            if( Program.GameMode.SupportsGuiChange )
            {
                this.Visible = true;

                if( this.UpdateGuis() )
                {
                    this.SetGui( Program.GameMode.CurrentGui );
                    this.loaded = true;
                }
            }
            else
                this.Visible = false;
        }

        internal void OnGuiChanged( object sender, EventArgs e )
        {
            string guiName = this.comboBox.SelectedItem.ToString();
            if( Program.GameMode.Colors.Loaded )
            {
                if( guiName == "Default" && this.comboBox.Items.Count > 1 && this.loaded )
                    Log.Information( Properties.Resources.DefaultGuiSelected );
                this.ReloadGui( guiName );
            }
        }

        internal void OnReloadClicked( object sender, EventArgs e )
        {
            this.ReloadGui();
        }

        private bool UpdateGuis()
        {
            this.comboBox.Items.Clear();
            string[] guiDirs = Program.GameMode.AvailableGuis;
            if( guiDirs != null )
            {
                this.comboBox.Items.AddRange( guiDirs );
                if( guiDirs.Length > 1 )
                {
                    this.Enabled = true;
                }
                else
                {
                    if( this.MaybeCreateGui( guiDirs[0] ) == true )
                        return false;
                    this.Enabled = false;
                }
            }
            else
            {
                this.Enabled = false;
            }
            return true;
        }

        public void SetGui( string gui )
        {
            Log.Debug( "[GuiBox.SetGui] GUI: {0}", gui );
            if( String.IsNullOrEmpty( gui ) )
                gui = "Default";

            if( gui == "Default" && this.comboBox.Items.Count > 1 )
                Log.Information( Properties.Resources.DefaultGuiSelected );

            if( this.comboBox.Items.Contains( gui ) )
            {
                Log.Debug( "[GuiBox.SetGui] Gui found." );
                this.comboBox.SelectedItem = gui;
            }
            else
            {
                Log.Debug( "[GuiBox.SetGui] Gui not found. Selecting Default." );
                this.comboBox.SelectedItem = "Default";
            }
        }

        private bool MaybeCreateGui( string gui )
        {
            if( gui != "Default" )
            {
                Log.Error( Properties.Resources.NoDefaultGui );
                return false;
            }
            else
            {
                DialogResult dr = MessageBox.Show( Properties.Resources.OnlyDefaultGui,
                                                   Properties.Resources.OnlyDefaultGuiCaption,
                                                   MessageBoxButtons.YesNo,
                                                   MessageBoxIcon.Question );
                if( dr == DialogResult.Yes )
                {
                    Program.GameMode.CreateGui( "Customized" );
                    this.UpdateGuis();
                    Program.GameMode.CurrentGui = "Customized";
                    this.SetGui( "Customized" );
                    this.loaded = true;

                    return true;
                }
            }
            return false;
        }

        internal void ReloadGui( string guiName = null )
        {
            if( this.loaded )
            {
                DialogResult dr = MessageBox.Show( Properties.Resources.ReloadText,
                                                   Properties.Resources.ReloadCaption,
                                                   MessageBoxButtons.YesNo,
                                                   MessageBoxIcon.Warning );
                if( dr == DialogResult.Yes )
                {
                    if( !String.IsNullOrEmpty( guiName ) )
                        Program.GameMode.CurrentGui = guiName;
                }
            }
            else
                Program.GameMode.CurrentGui = guiName;
        }
    }
}
