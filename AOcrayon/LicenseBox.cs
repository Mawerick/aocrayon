﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace AOcrayon
{
    /// <summary>
    /// Description of LicenseBox.
    /// </summary>
    public partial class LicenseBox : UserControl
    {
        public LicenseBox()
        {
            InitializeComponent();
            this.pictureBox.Image = Properties.Resources.Icon_GPL;
        }

        internal void OnLoad( object sender, EventArgs e )
        {
            if( this.DesignMode )
                return;
            try
            {
#if DEBUG
                StreamReader sr = File.OpenText( "..\\..\\Dist\\GPLv3_License.txt" );
#else
                StreamReader sr = File.OpenText( "GPLv3_License.txt" );
#endif
                this.richTextBox.Text = sr.ReadToEnd();
                sr.Close();
                sr.Dispose();
            }
            catch( FileNotFoundException )
            {
                this.richTextBox.Text = Properties.Resources.LicenseMissing;
                this.richTextBox.Enabled = false;
            }
        }
        
        internal void OnLinkClicked( object sender, LinkClickedEventArgs e )
        {
            Process.Start( e.LinkText );
        }
    }
}
