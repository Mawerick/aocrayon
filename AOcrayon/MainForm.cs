﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace AOcrayon
{
    /// <summary>
    /// Description of MainForm.
    /// </summary>
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            Program.Settings.AdvancedModeChanged += new EventHandler( OnModeChanged );
            Program.GameMode.Colors.ListChanged += new ListChangedEventHandler( OnColorsChanged );
            this.Text = String.Format( "{0} v{1} :: {2}", Application.ProductName, Application.ProductVersion, Program.GameMode.Name );
        }

        private void OnLoad( object sender, EventArgs e )
        {
            this.basicMode.Init();
            this.advancedMode.Init();
            this.OnModeChanged( sender, e );

            if( Program.Settings.Location != Point.Empty )
                this.Location = Program.Settings.Location;
        }

        private void OnClosing( object sender, FormClosingEventArgs e )
        {
            Program.Settings.Location = this.Location;
        }

        private void OnClosed( object sender, FormClosedEventArgs e )
        {
            Program.Quit();
        }

        private void OnCloseClicked( object sender, EventArgs e )
        {
            Program.Quit();
        }

        private void OnSaveClicked( object sender, EventArgs e )
        {
            this.progressBar.Visible = true;
            Program.GameMode.Colors.Save( this.progressBar );
            this.saveButton.Enabled = false;
            this.saveTimer.Start();
        }

        private void OnModeChanged( object sender, EventArgs e )
        {
            this.advancedMode.Visible = Program.Settings.AdvancedMode;
            this.basicMode.Visible = !Program.Settings.AdvancedMode;
        }
        
        private void OnColorsChanged( object sender, ListChangedEventArgs e )
        {
            this.saveButton.Enabled = true;
        }
        
        private void OnSaveTimerTick(object sender, EventArgs e)
        {
            if( this.progressBar.Visible )
            {
                this.progressBar.Visible = false;
                this.saveLabel.Visible = true;
                this.saveTimer.Interval = 1000;
            }
            else
            {
                this.saveTimer.Stop();
                this.saveLabel.Visible = false;
            }
        }
    }
}
