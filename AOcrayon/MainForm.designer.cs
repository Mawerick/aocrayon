﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOcrayon
{
    partial class MainForm
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Disposes resources used by the form.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if( components != null )
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );
        }

        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.colorsPage = new System.Windows.Forms.TabPage();
            this.basicMode = new AOcrayon.BasicMode();
            this.advancedMode = new AOcrayon.AdvancedMode();
            this.guiBox = new AOcrayon.GuiBox();
            this.settingsPage = new System.Windows.Forms.TabPage();
            this.settingsBox = new AOcrayon.SettingsBox();
            this.aboutPage = new System.Windows.Forms.TabPage();
            this.aboutBox = new AOcrayon.AboutBox();
            this.licensePage = new System.Windows.Forms.TabPage();
            this.licenseBox = new AOcrayon.LicenseBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.closeButton = new System.Windows.Forms.Button();
            this.saveTimer = new System.Windows.Forms.Timer(this.components);
            this.saveLabel = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.colorsPage.SuspendLayout();
            this.settingsPage.SuspendLayout();
            this.aboutPage.SuspendLayout();
            this.licensePage.SuspendLayout();
            this.SuspendLayout();
            //
            // tabControl
            //
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                                    | System.Windows.Forms.AnchorStyles.Left)
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.colorsPage);
            this.tabControl.Controls.Add(this.settingsPage);
            this.tabControl.Controls.Add(this.aboutPage);
            this.tabControl.Controls.Add(this.licensePage);
            this.tabControl.ItemSize = new System.Drawing.Size(0, 17);
            this.tabControl.Location = new System.Drawing.Point(6, 6);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(403, 452);
            this.tabControl.TabIndex = 0;
            //
            // colorsPage
            //
            this.colorsPage.Controls.Add(this.basicMode);
            this.colorsPage.Controls.Add(this.advancedMode);
            this.colorsPage.Controls.Add(this.guiBox);
            this.colorsPage.Location = new System.Drawing.Point(4, 21);
            this.colorsPage.Name = "colorsPage";
            this.colorsPage.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.colorsPage.Size = new System.Drawing.Size(395, 427);
            this.colorsPage.TabIndex = 0;
            this.colorsPage.Text = "Text Colors";
            this.colorsPage.UseVisualStyleBackColor = true;
            //
            // basicMode
            //
            this.basicMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.basicMode.Location = new System.Drawing.Point(0, 28);
            this.basicMode.Name = "basicMode";
            this.basicMode.Size = new System.Drawing.Size(395, 396);
            this.basicMode.TabIndex = 1;
            //
            // advancedMode
            //
            this.advancedMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.advancedMode.Location = new System.Drawing.Point(0, 28);
            this.advancedMode.Name = "advancedMode";
            this.advancedMode.Size = new System.Drawing.Size(395, 396);
            this.advancedMode.TabIndex = 0;
            //
            // guiBox
            //
            this.guiBox.BackColor = System.Drawing.Color.Transparent;
            this.guiBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.guiBox.Location = new System.Drawing.Point(0, 3);
            this.guiBox.Name = "guiBox";
            this.guiBox.Size = new System.Drawing.Size(395, 25);
            this.guiBox.TabIndex = 0;
            //
            // settingsPage
            //
            this.settingsPage.Controls.Add(this.settingsBox);
            this.settingsPage.Location = new System.Drawing.Point(4, 21);
            this.settingsPage.Name = "settingsPage";
            this.settingsPage.Padding = new System.Windows.Forms.Padding(3);
            this.settingsPage.Size = new System.Drawing.Size(395, 427);
            this.settingsPage.TabIndex = 1;
            this.settingsPage.Text = "Settings";
            this.settingsPage.UseVisualStyleBackColor = true;
            //
            // settingsBox
            //
            this.settingsBox.AutoSize = true;
            this.settingsBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settingsBox.Location = new System.Drawing.Point(3, 3);
            this.settingsBox.Name = "settingsBox";
            this.settingsBox.Size = new System.Drawing.Size(389, 421);
            this.settingsBox.TabIndex = 0;
            //
            // aboutPage
            //
            this.aboutPage.Controls.Add(this.aboutBox);
            this.aboutPage.Location = new System.Drawing.Point(4, 21);
            this.aboutPage.Name = "aboutPage";
            this.aboutPage.Size = new System.Drawing.Size(395, 427);
            this.aboutPage.TabIndex = 2;
            this.aboutPage.Text = "About...";
            this.aboutPage.UseVisualStyleBackColor = true;
            //
            // aboutBox
            //
            this.aboutBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.aboutBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aboutBox.Location = new System.Drawing.Point(0, 0);
            this.aboutBox.Name = "aboutBox";
            this.aboutBox.Size = new System.Drawing.Size(395, 427);
            this.aboutBox.TabIndex = 0;
            //
            // licensePage
            //
            this.licensePage.Controls.Add(this.licenseBox);
            this.licensePage.Location = new System.Drawing.Point(4, 21);
            this.licensePage.Name = "licensePage";
            this.licensePage.Padding = new System.Windows.Forms.Padding(3);
            this.licensePage.Size = new System.Drawing.Size(375, 427);
            this.licensePage.TabIndex = 3;
            this.licensePage.Text = "License";
            this.licensePage.UseVisualStyleBackColor = true;
            //
            // licenseBox
            //
            this.licenseBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.licenseBox.Location = new System.Drawing.Point(3, 3);
            this.licenseBox.Name = "licenseBox";
            this.licenseBox.Size = new System.Drawing.Size(369, 421);
            this.licenseBox.TabIndex = 0;
            //
            // saveButton
            //
            this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveButton.Enabled = false;
            this.saveButton.Location = new System.Drawing.Point(6, 462);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(85, 23);
            this.saveButton.TabIndex = 1;
            this.saveButton.Text = "&Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.OnSaveClicked);
            //
            // progressBar
            //
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(97, 462);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(221, 23);
            this.progressBar.TabIndex = 2;
            this.progressBar.Visible = false;
            //
            // closeButton
            //
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.Location = new System.Drawing.Point(324, 462);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(85, 23);
            this.closeButton.TabIndex = 3;
            this.closeButton.Text = "&Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.OnCloseClicked);
            //
            // saveTimer
            //
            this.saveTimer.Interval = 250;
            this.saveTimer.Tick += new System.EventHandler(this.OnSaveTimerTick);
            //
            // saveLabel
            //
            this.saveLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.saveLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveLabel.Location = new System.Drawing.Point(97, 462);
            this.saveLabel.Name = "saveLabel";
            this.saveLabel.Size = new System.Drawing.Size(221, 23);
            this.saveLabel.TabIndex = 4;
            this.saveLabel.Text = "Save completed!";
            this.saveLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.saveLabel.Visible = false;
            //
            // MainForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 492);
            this.Controls.Add(this.saveLabel);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.closeButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "AOcrayon";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OnClosed);
            this.Load += new System.EventHandler(this.OnLoad);
            this.tabControl.ResumeLayout(false);
            this.colorsPage.ResumeLayout(false);
            this.settingsPage.ResumeLayout(false);
            this.settingsPage.PerformLayout();
            this.aboutPage.ResumeLayout(false);
            this.licensePage.ResumeLayout(false);
            this.ResumeLayout(false);
        }
        private System.Windows.Forms.Label saveLabel;
        private System.Windows.Forms.Timer saveTimer;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage colorsPage;
        private GuiBox guiBox;
        private AOcrayon.BasicMode basicMode;
        private AOcrayon.AdvancedMode advancedMode;
        private System.Windows.Forms.TabPage settingsPage;
        private AOcrayon.SettingsBox settingsBox;
        private System.Windows.Forms.TabPage aboutPage;
        private AOcrayon.AboutBox aboutBox;
        private System.Windows.Forms.TabPage licensePage;
        private AOcrayon.LicenseBox licenseBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button closeButton;
    }
}
