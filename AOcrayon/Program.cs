﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Threading;
using System.Windows.Forms;

using FCGame;
using Twp.Controls;
using Twp.Utilities;

namespace AOcrayon
{
    /// <summary>
    /// Class with program entry point.
    /// </summary>
    class Program : ApplicationContext
    {
        #region Static members

        private static Program appContext;

        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
#if !DEBUG
            Application.ThreadException += new ThreadExceptionEventHandler( OnThreadException );
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler( OnUnhandledException );
#endif
            appContext = new Program();
            appContext.Start();
            if( appContext.GameModeSelected )
                Application.Run( appContext );
            else
                return;
        }

        public static void Restart()
        {
            if( appContext != null )
            {
                appContext.Stop();
                appContext.SelectMode();
                appContext.Start();
            }
        }

        public static void Quit()
        {
            appContext.ExitThread();
        }

        private static Settings settings = new Settings();
        public static Settings Settings
        {
            get { return settings; }
        }

        private static GameModeManager gameModes = new GameModeManager();
        public static GameModeManager GameModes
        {
            get { return gameModes; }
        }

        private static IGameMode gameMode;
        public static IGameMode GameMode
        {
            get { return gameMode; }
        }

#if !DEBUG
        private static void OnThreadException( object sender, ThreadExceptionEventArgs e )
        {
            ExceptionDialog.Show( e.Exception );
        }

        private static void OnUnhandledException( object sender, UnhandledExceptionEventArgs e )
        {
            ExceptionDialog.Show( e.ExceptionObject as Exception );
        }
#endif

        #endregion

        #region Non-static members

        public Program()
        {
            Application.ApplicationExit += new EventHandler( OnApplicationExit );

            Log.Start( "log.txt", Path.GetAppDataPath() );
#if DEBUG
            Log.DebugOutput |= Log.OutputType.Console;
            this.consoleWindow.Show();
#endif
            Settings.Load();
            this.Init();
        }
        
#if DEBUG
        private ConsoleWindow consoleWindow = new ConsoleWindow();
#endif

        private void Init()
        {
            Log.Debug( "[Program.Init] Detecting Game modes..." );
            GameModes.FindModes();
            Log.Debug( "[Program.Init] Total modes: {0}", GameModes.Modes.Count );
            if( GameModes.Modes.Count < 1 )
            {
                Log.Fatal( "No Game modes found! Try re-installing the application." );
            }
            else if( GameModes.Modes.Count > 1 )
            {
                Log.Debug( "[Program.Init] Remember mode? {0}", Settings.RememberMode );
                Log.Debug( "[Program.Init] Last mode used: {0}", Settings.LastGameMode );
                if( Settings.RememberMode && GameModes.Modes.Find( Settings.LastGameMode ) != null )
                {
                    this.gameModeSelected = true;
                }
                else
                {
                    this.SelectMode();
                }
            }
            else
            {
                Settings.LastGameMode = GameModes.Modes.First().GameID;
                this.gameModeSelected = true;
            }
        }
        
        private bool gameModeSelected = false;
        public bool GameModeSelected
        {
            get { return gameModeSelected; }
        }

        public void Start()
        {
            Log.Debug( "[Program.Start] GameModeSelected: {0}", this.gameModeSelected );
            if( !this.gameModeSelected )
            {
                ExitThread();
                return;
            }
            
            Log.Debug( "[Program.Start] LastGameMode: {0}", Settings.LastGameMode );

            gameMode = GameModes.Modes.Find( Settings.LastGameMode );
            if( gameMode == null )
            {
                Log.Error( "Failed to load game mode {0}", Settings.LastGameMode );
                Restart();
                return;
            }

            if( !gameMode.Initialize( Settings[gameMode.GameID] ) )
            {
                Log.Error( "Failed to initialize game mode {0}", Settings.LastGameMode );
                Restart();
                return;
            }

            this.mainForm = new MainForm();
            this.mainForm.Show();
        }

        public void Stop()
        {
            if( this.mainForm != null )
            {
                this.mainForm.Dispose();
                this.mainForm = null;
            }
            gameMode.Cleanup();
        }

        public void SelectMode()
        {
            this.gameModeSelected = false;
            GameModeSelectionForm gmsForm = new GameModeSelectionForm();
            DialogResult dr = gmsForm.ShowDialog();
            if( dr == DialogResult.OK )
                this.gameModeSelected = true;
        }

        private void OnApplicationExit( object sender, EventArgs e )
        {
            Log.Debug( "[Program.OnApplicationExit] Saving settings..." );
            Settings.Save();
        }

        private MainForm mainForm;

        #endregion
    }
}
