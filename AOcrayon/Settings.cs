// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;

using Twp.Utilities;

namespace AOcrayon
{
    public class Settings : FCGame.ISettings
    {
        private IniDocument doc = new IniDocument( "AOcrayon.ini", Path.GetAppDataPath() );

        public event EventHandler AdvancedModeChanged;
        public event EventHandler BackgroundColorChanged;

        public bool RememberMode
        {
            get { return Convert.ToBoolean( this.doc["GameMode"]["RememberMode"] ); }
            set { this.doc["GameMode"]["RememberMode"] = Convert.ToString( value ); }
        }

        public string LastGameMode
        {
            get { return this.doc["GameMode"]["LastGameMode"]; }
            set { this.doc["GameMode"]["LastGameMode"] = value; }
        }
        
        private int[] customColors;
        public int[] CustomColors
        {
            get { return this.customColors; }
            set { this.customColors = value; }
        }

        public bool AdvancedMode
        {
            get { return Convert.ToBoolean( this.doc["Interface"]["AdvancedMode"] ); }
            set
            {
                if( this.AdvancedMode != value )
                {
                    this.doc["Interface"]["AdvancedMode"] = Convert.ToString( value );
                    if( this.AdvancedModeChanged != null )
                        this.AdvancedModeChanged( this, EventArgs.Empty );
                }
            }
        }

        public Color BackgroundColor
        {
            get { return ColorTranslator.FromHtml( this.doc["Interface"]["BackgroundColor"] ); }
            set
            {
                if( this.BackgroundColor != value )
                {
                    this.doc["Interface"]["BackgroundColor"] = ColorTranslator.ToHtml( value );
                    if( this.BackgroundColorChanged != null )
                        this.BackgroundColorChanged( this, EventArgs.Empty );
                }
            }
        }

        public Point Location
        {
            get
            {
                if( String.IsNullOrEmpty( this.doc["Interface"]["Location"] ) )
                    return Point.Empty;
                TypeConverter converter = TypeDescriptor.GetConverter( typeof( Point ) );
                return (Point) converter.ConvertFromString( this.doc["Interface"]["Location"] );
            }
            set
            {
                TypeConverter converter = TypeDescriptor.GetConverter( typeof( Point ) );
                this.doc["Interface"]["Location"] = converter.ConvertToString( value );
            }
        }

        public void Load()
        {
            try
            {
                this.doc.Read();
            }
            catch( IniParseException ex )
            {
                Log.Warning( ex.Message );
                Log.Debug( "[Settings.Load]", ex.Line );
            }

            this.doc.SetDefault( "GameMode", "RememberMode", "false" );
            this.doc.SetDefault( "GameMode", "LastGameMode", "" );
            this.doc.SetDefault( "Interface", "AdvancedMode", "true" );
            this.doc.SetDefault( "Interface", "BackgroundColor", "#333333" );
            
            List<int> cc = new List<int>();
            foreach( string color in this.doc["CustomColors"].Values )
            {
                cc.Add( int.Parse( color.Substring( 1 ), System.Globalization.NumberStyles.HexNumber ) );
            }
            this.customColors = cc.ToArray();
        }

        public void Save()
        {
            if( this.customColors != null )
            {
                this.doc["CustomColors"].Clear();
                int i = 1;
                foreach( int color in this.customColors )
                {
                    if( color != 0xffffff )
                        this.doc["CustomColors"].Add( String.Format( "Color{0}", i++ ), "#" + color.ToString( "X6" ) );
                }
            }
            this.doc.Write();
        }

        public IniSection this[string name]
        {
            get { return this.doc[name]; }
        }
    }
}
