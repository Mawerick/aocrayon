﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOcrayon
{
    partial class SettingsBox
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Disposes resources used by the control.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if( components != null )
                {
                    components.Dispose();
                }
            }
            base.Dispose( disposing );
        }

        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.pathToolBox = new System.Windows.Forms.GroupBox();
            this.pathBox = new System.Windows.Forms.TextBox();
            this.pathButton = new System.Windows.Forms.Button();
            this.backupToolBox = new System.Windows.Forms.GroupBox();
            this.restoreBackupButton = new System.Windows.Forms.Button();
            this.restoreDefaultsButton = new System.Windows.Forms.Button();
            this.tweakToolBox = new System.Windows.Forms.GroupBox();
            this.saveGuiButton = new System.Windows.Forms.Button();
            this.miscToolBox = new System.Windows.Forms.GroupBox();
            this.backgroundLabel = new System.Windows.Forms.Label();
            this.backgroundButton = new System.Windows.Forms.Button();
            this.rememberGameModeBox = new System.Windows.Forms.CheckBox();
            this.changeGameModeButton = new System.Windows.Forms.Button();
            this.advancedModeButton = new System.Windows.Forms.CheckBox();
            this.pathToolBox.SuspendLayout();
            this.backupToolBox.SuspendLayout();
            this.tweakToolBox.SuspendLayout();
            this.miscToolBox.SuspendLayout();
            this.SuspendLayout();
            //
            // colorDialog
            //
            this.colorDialog.FullOpen = true;
            //
            // pathToolBox
            //
            this.pathToolBox.Controls.Add(this.pathBox);
            this.pathToolBox.Controls.Add(this.pathButton);
            this.pathToolBox.Location = new System.Drawing.Point(0, 0);
            this.pathToolBox.Name = "pathToolBox";
            this.pathToolBox.Size = new System.Drawing.Size(359, 49);
            this.pathToolBox.TabIndex = 0;
            this.pathToolBox.TabStop = false;
            this.pathToolBox.Text = "Game install path";
            //
            // pathBox
            //
            this.pathBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.pathBox.Enabled = false;
            this.pathBox.Location = new System.Drawing.Point(6, 19);
            this.pathBox.Name = "pathBox";
            this.pathBox.Size = new System.Drawing.Size(315, 20);
            this.pathBox.TabIndex = 0;
            //
            // pathButton
            //
            this.pathButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pathButton.Location = new System.Drawing.Point(327, 15);
            this.pathButton.Name = "pathButton";
            this.pathButton.Size = new System.Drawing.Size(26, 26);
            this.pathButton.TabIndex = 1;
            this.pathButton.UseVisualStyleBackColor = true;
            this.pathButton.Click += new System.EventHandler(this.OnBrowseClicked);
            //
            // backupToolBox
            //
            this.backupToolBox.Controls.Add(this.restoreBackupButton);
            this.backupToolBox.Controls.Add(this.restoreDefaultsButton);
            this.backupToolBox.Location = new System.Drawing.Point(0, 55);
            this.backupToolBox.Name = "backupToolBox";
            this.backupToolBox.Size = new System.Drawing.Size(359, 80);
            this.backupToolBox.TabIndex = 1;
            this.backupToolBox.TabStop = false;
            this.backupToolBox.Text = "Restore colors from backup";
            //
            // restoreBackupButton
            //
            this.restoreBackupButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.restoreBackupButton.Location = new System.Drawing.Point(6, 19);
            this.restoreBackupButton.Name = "restoreBackupButton";
            this.restoreBackupButton.Size = new System.Drawing.Size(347, 23);
            this.restoreBackupButton.TabIndex = 0;
            this.restoreBackupButton.Text = "Restore last backup";
            this.restoreBackupButton.UseVisualStyleBackColor = true;
            this.restoreBackupButton.Click += new System.EventHandler(this.OnRestoreBackupClicked);
            //
            // restoreDefaultsButton
            //
            this.restoreDefaultsButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.restoreDefaultsButton.Location = new System.Drawing.Point(6, 48);
            this.restoreDefaultsButton.Name = "restoreDefaultsButton";
            this.restoreDefaultsButton.Size = new System.Drawing.Size(347, 23);
            this.restoreDefaultsButton.TabIndex = 1;
            this.restoreDefaultsButton.Text = "Restore default colors";
            this.restoreDefaultsButton.UseVisualStyleBackColor = true;
            this.restoreDefaultsButton.Click += new System.EventHandler(this.OnRestoreDefaultsClicked);
            //
            // tweakToolBox
            //
            this.tweakToolBox.Controls.Add(this.saveGuiButton);
            this.tweakToolBox.Location = new System.Drawing.Point(0, 141);
            this.tweakToolBox.Name = "tweakToolBox";
            this.tweakToolBox.Size = new System.Drawing.Size(359, 49);
            this.tweakToolBox.TabIndex = 2;
            this.tweakToolBox.TabStop = false;
            this.tweakToolBox.Text = "Preferences tweaks";
            //
            // saveGuiButton
            //
            this.saveGuiButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                                    | System.Windows.Forms.AnchorStyles.Right)));
            this.saveGuiButton.Location = new System.Drawing.Point(6, 19);
            this.saveGuiButton.Name = "saveGuiButton";
            this.saveGuiButton.Size = new System.Drawing.Size(347, 23);
            this.saveGuiButton.TabIndex = 2;
            this.saveGuiButton.Text = "Save GUI selection";
            this.saveGuiButton.UseVisualStyleBackColor = true;
            this.saveGuiButton.Click += new System.EventHandler(this.OnSaveGuiButtonClicked);
            //
            // miscToolBox
            //
            this.miscToolBox.Controls.Add(this.backgroundLabel);
            this.miscToolBox.Controls.Add(this.backgroundButton);
            this.miscToolBox.Controls.Add(this.rememberGameModeBox);
            this.miscToolBox.Controls.Add(this.changeGameModeButton);
            this.miscToolBox.Controls.Add(this.advancedModeButton);
            this.miscToolBox.Location = new System.Drawing.Point(0, 196);
            this.miscToolBox.Name = "miscToolBox";
            this.miscToolBox.Size = new System.Drawing.Size(359, 104);
            this.miscToolBox.TabIndex = 3;
            this.miscToolBox.TabStop = false;
            this.miscToolBox.Text = "Miscellaneous";
            //
            // backgroundLabel
            //
            this.backgroundLabel.AutoSize = true;
            this.backgroundLabel.Location = new System.Drawing.Point(6, 24);
            this.backgroundLabel.Name = "backgroundLabel";
            this.backgroundLabel.Size = new System.Drawing.Size(94, 13);
            this.backgroundLabel.TabIndex = 0;
            this.backgroundLabel.Text = "Background color:";
            //
            // backgroundButton
            //
            this.backgroundButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.backgroundButton.BackColor = System.Drawing.SystemColors.ControlText;
            this.backgroundButton.Location = new System.Drawing.Point(192, 19);
            this.backgroundButton.Name = "backgroundButton";
            this.backgroundButton.Size = new System.Drawing.Size(161, 23);
            this.backgroundButton.TabIndex = 1;
            this.backgroundButton.UseVisualStyleBackColor = false;
            this.backgroundButton.Click += new System.EventHandler(this.OnBackgroundClicked);
            //
            // rememberGameModeBox
            //
            this.rememberGameModeBox.AutoSize = true;
            this.rememberGameModeBox.Location = new System.Drawing.Point(8, 52);
            this.rememberGameModeBox.Name = "rememberGameModeBox";
            this.rememberGameModeBox.Size = new System.Drawing.Size(135, 17);
            this.rememberGameModeBox.TabIndex = 2;
            this.rememberGameModeBox.Text = "Remember game mode";
            this.rememberGameModeBox.UseVisualStyleBackColor = true;
            this.rememberGameModeBox.CheckedChanged += new System.EventHandler(this.OnRememberGameModeChanged);
            //
            // changeGameModeButton
            //
            this.changeGameModeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.changeGameModeButton.Location = new System.Drawing.Point(192, 48);
            this.changeGameModeButton.Name = "changeGameModeButton";
            this.changeGameModeButton.Size = new System.Drawing.Size(161, 23);
            this.changeGameModeButton.TabIndex = 3;
            this.changeGameModeButton.Text = "Change game mode";
            this.changeGameModeButton.UseVisualStyleBackColor = true;
            this.changeGameModeButton.Click += new System.EventHandler(this.OnChangeGameMode);
            //
            // advancedModeButton
            //
            this.advancedModeButton.AutoSize = true;
            this.advancedModeButton.Location = new System.Drawing.Point(8, 77);
            this.advancedModeButton.Name = "advancedModeButton";
            this.advancedModeButton.Size = new System.Drawing.Size(104, 17);
            this.advancedModeButton.TabIndex = 4;
            this.advancedModeButton.Text = "Advanced mode";
            this.advancedModeButton.UseVisualStyleBackColor = true;
            this.advancedModeButton.CheckedChanged += new System.EventHandler(this.OnModeChanged);
            //
            // SettingsBox
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pathToolBox);
            this.Controls.Add(this.backupToolBox);
            this.Controls.Add(this.tweakToolBox);
            this.Controls.Add(this.miscToolBox);
            this.Name = "SettingsBox";
            this.Size = new System.Drawing.Size(359, 399);
            this.Load += new System.EventHandler(this.OnLoad);
            this.pathToolBox.ResumeLayout(false);
            this.pathToolBox.PerformLayout();
            this.backupToolBox.ResumeLayout(false);
            this.tweakToolBox.ResumeLayout(false);
            this.miscToolBox.ResumeLayout(false);
            this.miscToolBox.PerformLayout();
            this.ResumeLayout(false);
        }
        private System.Windows.Forms.Button saveGuiButton;

        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.GroupBox pathToolBox;
        private System.Windows.Forms.TextBox pathBox;
        private System.Windows.Forms.Button pathButton;
        private System.Windows.Forms.GroupBox backupToolBox;
        private System.Windows.Forms.Button restoreBackupButton;
        private System.Windows.Forms.Button restoreDefaultsButton;
        private System.Windows.Forms.GroupBox tweakToolBox;
        private System.Windows.Forms.GroupBox miscToolBox;
        private System.Windows.Forms.Label backgroundLabel;
        private System.Windows.Forms.Button backgroundButton;
        private System.Windows.Forms.CheckBox rememberGameModeBox;
        private System.Windows.Forms.Button changeGameModeButton;
        private System.Windows.Forms.CheckBox advancedModeButton;
    }
}
