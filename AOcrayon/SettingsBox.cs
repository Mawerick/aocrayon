﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Windows.Forms;
using System.Windows.Forms.Layout;

using Twp.Controls;

namespace AOcrayon
{
    /// <summary>
    /// Description of SettingsPage.
    /// </summary>
    public partial class SettingsBox : UserControl
    {
        public SettingsBox()
        {
            InitializeComponent();

            Program.Settings.BackgroundColorChanged += new EventHandler( OnBackgroundColorChanged );
        }

        protected override void OnLayout( LayoutEventArgs e )
        {
            base.OnLayout( e );
            if(  !this.DesignMode && Program.GameMode.SupportsGuiChange )
            {
                this.toolTip.SetToolTip( this.saveGuiButton, String.Format( Properties.Resources.SaveGuiToolTip, Program.GameMode.CurrentGui ) );
            }
        }

        private VerticalLayout layoutEngine;
        public override LayoutEngine LayoutEngine
        {
            get
            {
                if( this.layoutEngine == null )
                {
                    this.layoutEngine = new VerticalLayout();
                    this.layoutEngine.HorizontalAlign = HorizontalLayoutAlignment.Stretch;
                }
                return this.layoutEngine;
            }
        }

        private void OnLoad( object sender, EventArgs e )
        {
            if( this.DesignMode )
                return;

            this.pathBox.Text = Program.GameMode.Path;
            this.tweakToolBox.Visible = Program.GameMode.SupportsGuiChange;
            this.backgroundButton.BackColor = Program.Settings.BackgroundColor;
            this.rememberGameModeBox.Checked = Program.Settings.RememberMode;
            this.advancedModeButton.Checked = Program.Settings.AdvancedMode;

            if( Program.GameModes.Modes.Count <= 1 )
            {
                this.rememberGameModeBox.Visible = false;
                this.changeGameModeButton.Visible = false;
            }
        }


        #region Install Path

        internal void OnBrowseClicked( object sender, EventArgs e )
        {
            if( Program.GameMode.Browse() )
                pathBox.Text = Program.GameMode.Path;
        }
        
        #endregion

        #region Backup

        internal void OnRestoreBackupClicked( object sender, EventArgs e )
        {
            Program.GameMode.Colors.Restore();
        }

        internal void OnRestoreDefaultsClicked( object sender, EventArgs e )
        {
            Program.GameMode.Colors.RestoreDefaults();
        }

        #endregion

        #region Prefs tweaks

        private void OnSaveGuiButtonClicked(object sender, EventArgs e)
        {
            if( Program.GameMode.SupportsGuiChange )
            {
                Program.GameMode.WritePrefs();
            }
        }

        #endregion

        #region Misc

        private void OnBackgroundClicked( object sender, EventArgs e )
        {
            this.colorDialog.Color = Program.Settings.BackgroundColor;
            DialogResult res = this.colorDialog.ShowDialog();
            if( res == DialogResult.OK )
            {
                Program.Settings.BackgroundColor = this.colorDialog.Color;
            }
        }

        private void OnBackgroundColorChanged( object sender, EventArgs e )
        {
            this.backgroundButton.BackColor = Program.Settings.BackgroundColor;
        }

        private void OnRememberGameModeChanged( object sender, EventArgs e )
        {
            Program.Settings.RememberMode = this.rememberGameModeBox.Checked;
        }

        private void OnChangeGameMode( object sender, EventArgs e )
        {
            DialogResult dr = MessageBox.Show( Properties.Resources.ChangeGameModeText,
                                               Properties.Resources.ChangeGameModeCaption,
                                               MessageBoxButtons.YesNo,
                                               MessageBoxIcon.Warning );
            if( dr == DialogResult.Yes )
            {
                Program.Restart();
            }
        }

        private void OnModeChanged( object sender, EventArgs e )
        {
            Program.Settings.AdvancedMode = this.advancedModeButton.Checked;
        }
        
        #endregion
    }
}
