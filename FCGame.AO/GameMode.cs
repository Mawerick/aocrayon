﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Xml;
using Twp.FC.AO;
using Twp.Utilities;

namespace FCGame.AO
{
    public class GameMode : IGameMode
    {
        #region Properties

        public string Name
        {
            get { return "Anarchy Online"; }
        }

        public string GameID
        {
            get { return "AO"; }
        }

        private Settings settings = new Settings();

        public string Path
        {
            get { return this.settings.GamePath; }
        }

        public bool SupportsGuiChange
        {
            get { return true; }
        }

        public string[] AvailableGuis
        {
            get { return Gui.List( this.Path ); }
        }

        public string CurrentGui
        {
            get { return this.gui.Name; }
            set
            {
                if( this.gui.Name != value )
                {
                    this.gui = new Gui( this.Path, value );
                    this.colors.Load( this.XmlFile );
                }
            }
        }

        private Twp.FC.XmlFile prefs;
        private Gui gui;

        public Twp.FC.XmlFile XmlFile
        {
            get { return this.gui.TextColors; }
        }

        private TextColors colors = new TextColors();
        public TextColors Colors
        {
            get { return this.colors; }
        }

        private ColorCategories categories;
        public IColorCategories Categories
        {
            get { return this.categories; }
        }

        public System.Resources.ResourceManager Resources
        {
            get { return Properties.Resources.ResourceManager; }
        }

        #endregion

        #region Methods

        public bool Initialize( IniSection settings )
        {
            this.settings.Init( settings );
            this.categories = new ColorCategories();

            Log.Debug( "[GameMode.Initialize] Verifying game path." );
            string path = this.settings.GamePath;
            if( !GamePath.Confirm( ref path ) )
                return false;

            this.settings.GamePath = path;

            if( !Prefs.IsValid( path ) )
                return false;

            this.prefs = Prefs.Read( path );
            this.gui = new Gui( path, this.prefs );

            Log.Debug( "[GameMode.Initialize] Loading TextColors." );
            this.colors.Load( this.XmlFile );
            this.colors.DefaultFileName = Gui.Default( path ).TextColors.FileName;

            Log.Debug( "[GameMode.Initialize] Done." );
            return true;
        }

        public bool WritePrefs()
        {
            XmlElement element = this.prefs.GetElement( "Value", "GUIName" );
            if( element != null )
            {
                element.Attributes["value"].Value = Twp.FC.XmlFile.AddQuotes( this.gui.Name );
                this.prefs.Save();
                return true;
            }
            return false;
        }

        public void Cleanup()
        {
            this.categories.Items.Clear();
            this.categories = null;
        }

        public void Dispose()
        {
            this.gui = null;
            this.prefs = null;
        }

        public bool Browse()
        {
            string path = this.settings.GamePath;
            if( !GamePath.Browse( ref path ) )
                return false;

            this.settings.GamePath = path;
            return true;
        }
        
        public bool CreateGui( string name )
        {
            if( String.IsNullOrEmpty( name ) )
                throw new ArgumentNullException( name );
            
            if( Gui.IsValid( this.settings.GamePath, name ) )
                return true;

            if( !Gui.CreateDirectory( this.settings.GamePath, name ) )
                return false;
            
            return Gui.Copy( this.settings.GamePath, "Default", name /*, new string[] { "TextColors.xml" } */ );
        }

        public override string ToString()
        {
            return this.Name;
        }

        #endregion

        #region Unsupported
        
        public bool SupportsSplitColors
        {
            get { return false; }
        }

        #endregion
    }
}
