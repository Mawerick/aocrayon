﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.Collections.Generic;

namespace FCGame.AoC
{
    public class ColorCategories : IColorCategories
    {
        public ColorCategories()
        {
            Category chat = new Category( "Chat" );
            chat.Items.Add( new ColorGroup( "Guild chat", new string[] { "ctch_clan_name", "ctch_clan_text" } ) );
            chat.Items.Add( new ColorGroup( "Raid chat", new string[] { "ctch_raid_name", "ctch_raid_text" } ) );
            chat.Items.Add( new ColorGroup( "Team chat", new string[] { "ctch_team_name", "ctch_team_text" } ) );
            chat.Items.Add( new ColorGroup( "Private groups", new string[] { "ctch_pgroup_name", "ctch_pgroup_text" } ) );
            chat.Items.Add( new ColorGroup( "Private messages", new string[] { "ctch_tell_name", "ctch_tell_text" } ) );
            chat.Items.Add( new ColorGroup( "OOC", new string[] { "ctch_newbie_name", "ctch_newbie_text" } ) );
            chat.Items.Add( new ColorGroup( "News", new string[] { "ctch_news_name", "ctch_news_text" } ) );
            chat.Items.Add( new ColorGroup( "Misc.", "This is used various places, among others Org Leaders chat, and Social chat on TestLive", new string[] { "ctch_misc_name", "ctch_misc_text" } ) );
            this.items.Add( chat );

            Category moreChat = new Category( "Chat (cont.)" );
            moreChat.Items.Add( new ColorGroup( "Vicinity", new string[] { "ctch_vicinity_name", "ctch_vicinity_text" } ) );
            moreChat.Items.Add( new ColorGroup( "Whispers", new string[] { "ctch_whisper_name", "ctch_whisper_text" } ) );
            moreChat.Items.Add( new ColorGroup( "Shouts", new string[] { "ctch_shout_name", "ctch_shout_text" } ) );
            moreChat.Items.Add( new ColorGroup( "Emotes", new string[] { "ctch_emote_name", "ctch_emote_text" } ) );
            moreChat.Items.Add( new ColorGroup( "Shopping channels", new string[] { "ctch_seekingteam_name", "ctch_seekingteam_text" } ) );
            moreChat.Items.Add( new ColorGroup( "Your pet", new string[] { "ctch_mypet" } ) );
            moreChat.Items.Add( new ColorGroup( "Other pets", new string[] { "ctch_otherpet" } ) );
            this.items.Add( moreChat );
//
//            Category items = new Category( "Items" );
//            items.Items.Add( new ColorGroup( "Mundane", new string[] { "mundane" } ) );
//            items.Items.Add( new ColorGroup( "Superior", new string[] { "superior" } ) );
//            items.Items.Add( new ColorGroup( "Enchanted (World Drop)", new string[] { "enchantedlight" } ) );
//            items.Items.Add( new ColorGroup( "Enchanted", new string[] { "enchanted" } ) );
//            items.Items.Add( new ColorGroup( "Rare (World Drop)", new string[] { "rarelight" } ) );
//            items.Items.Add( new ColorGroup( "Rare", new string[] { "rare" } ) );
//            items.Items.Add( new ColorGroup( "Epic (World Drop)", new string[] { "epiclight" } ) );
//            items.Items.Add( new ColorGroup( "Epic", new string[] { "epic" } ) );
//            items.Items.Add( new ColorGroup( "Legendary", new string[] { "legendary" } ) );
//            this.items.Add( items );
//
//            Category money = new Category( "Money" );
//            money.Items.Add( new ColorGroup( "Tin", new string[] { "money_tin" } ) );
//            money.Items.Add( new ColorGroup( "Copper", new string[] { "money_copper" } ) );
//            money.Items.Add( new ColorGroup( "Silver", new string[] { "money_silver" } ) );
//            money.Items.Add( new ColorGroup( "Gold", new string[] { "money_gold" } ) );
//            money.Items.Add( new ColorGroup( "Rank: Bronze", new string[] { "bronze_rank" } ) );
//            money.Items.Add( new ColorGroup( "Rank: Silver", new string[] { "silver_rank" } ) );
//            money.Items.Add( new ColorGroup( "Rank: Gold", new string[] { "gold_rank" } ) );
//            this.items.Add( money );

            Category combatSelf = new Category( "Combat (Self)" );
            combatSelf.Items.Add( new FontColorGroup( "Normal hit", new string[] { "self_attacked" } ) );
            combatSelf.Items.Add( new FontColorGroup( "Unshielded hit", new string[] { "self_attacked_unshielded" } ) );
            combatSelf.Items.Add( new FontColorGroup( "Critical hit", new string[] { "self_attacked_critical" } ) );
            combatSelf.Items.Add( new FontColorGroup( "Spell: Normal hit", new string[] { "self_attacked_spell" } ) );
            combatSelf.Items.Add( new FontColorGroup( "Spell: Critical hit", new string[] { "self_attacked_spell_critical" } ) );
            combatSelf.Items.Add( new FontColorGroup( "Combo: Normal hit", new string[] { "self_attacked_combo" } ) );
            combatSelf.Items.Add( new FontColorGroup( "Combo: Critical hit", new string[] { "self_attacked_combo_critical" } ) );
            combatSelf.Items.Add( new FontColorGroup( "Combo: Name", new string[] { "self_combo_name" } ) );
            combatSelf.Items.Add( new FontColorGroup( "Environment damage", new string[] { "self_attacked_environment" } ) );
            this.items.Add( combatSelf );

            Category combatOther = new Category( "Combat (Other)" );
            combatOther.Items.Add( new FontColorGroup( "Normal hit", new string[] { "other_attacked" } ) );
            combatOther.Items.Add( new FontColorGroup( "Unshielded hit", new string[] { "other_attacked_unshielded" } ) );
            combatOther.Items.Add( new FontColorGroup( "Critical hit", new string[] { "other_attacked_critical" } ) );
            combatOther.Items.Add( new FontColorGroup( "Spell: Normal hit", new string[] { "other_attacked_spell" } ) );
            combatOther.Items.Add( new FontColorGroup( "Spell: Critical hit", new string[] { "other_attacked_spell_critical" } ) );
            combatOther.Items.Add( new FontColorGroup( "Combo: Normal hit", new string[] { "other_attacked_combo" } ) );
            combatOther.Items.Add( new FontColorGroup( "Combo: Critical hit", new string[] { "other_attacked_combo_critical" } ) );
            combatOther.Items.Add( new FontColorGroup( "Combo: Name", new string[] { "other_combo_name" } ) );
            combatOther.Items.Add( new FontColorGroup( "Environment damage", new string[] { "other_attacked_environment" } ) );
            this.items.Add( combatOther );

            Category healing = new Category( "Healing" );
            healing.Items.Add( new FontColorGroup( "Self healed", new string[] { "self_healed" } ) );
            healing.Items.Add( new FontColorGroup( "Self healed (Critical)", new string[] { "self_healed_critical" } ) );
            healing.Items.Add( new FontColorGroup( "Other healed", new string[] { "other_healed" } ) );
            healing.Items.Add( new FontColorGroup( "Other healed (Critical)", new string[] { "other_healed_critical" } ) );
            healing.Items.Add( new FontColorGroup( "Mana gained", new string[] { "mana_gained" } ) );
            healing.Items.Add( new FontColorGroup( "Mana gained (Critical)", new string[] { "mana_gained_critical" } ) );
            healing.Items.Add( new FontColorGroup( "Mana lost", new string[] { "mana_lost" } ) );
            healing.Items.Add( new FontColorGroup( "Mana lost (Critical)", new string[] { "mana_loss_critical" } ) );
            this.items.Add( healing );
            
            Category misc = new Category( "Misc." );
            misc.Items.Add( new FontColorGroup( "Self dodged", new string[] { "self_dodged" } ) );
            misc.Items.Add( new FontColorGroup( "Other dodged", new string[] { "other_dodged" } ) );
            misc.Items.Add( new FontColorGroup( "Stamina gained", new string[] { "stamina_gained" } ) );
            misc.Items.Add( new FontColorGroup( "Stamina gained (Critical)", new string[] { "stamina_gained_critical" } ) );
            misc.Items.Add( new FontColorGroup( "Stamina lost", new string[] { "stamina_lost" } ) );
            misc.Items.Add( new FontColorGroup( "Stamina lost (Critical)", new string[] { "stamina_loss_critical" } ) );
            misc.Items.Add( new ColorGroup( "XP gained", new string[] { "xp_gained" } ) );
            misc.Items.Add( new ColorGroup( "System messages", new string[] { "ct_system" } ) );
            this.items.Add( misc );
        }

        private List<Category> items = new List<Category>();
        public List<Category> Items
        {
            get { return this.items; }
        }
    }
}
