﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.Collections.Generic;

namespace FCGame.TSW
{
    public class ColorCategories : IColorCategories
    {
        public ColorCategories()
        {
            Category chat = new Category( "Chat" );
            chat.Items.Add( new NamedColorGroup( "Cabal chat", new string[] { "ctch_clan_name", "ctch_clan_text" }, new string[] { "Name", "Text" } ) );
            chat.Items.Add( new NamedColorGroup( "Cabal Officer chat", new string[] { "ctch_clanofficer_name", "ctch_clanofficer_text" }, new string[] { "Name", "Text" } ) );
            chat.Items.Add( new NamedColorGroup( "Raid chat", new string[] { "ctch_raid_name", "ctch_raid_text" }, new string[] { "Name", "Text" } ) );
            chat.Items.Add( new NamedColorGroup( "Team chat", new string[] { "ctch_team_name", "ctch_team_text" }, new string[] { "Name", "Text" } ) );
            chat.Items.Add( new NamedColorGroup( "Private groups", new string[] { "ctch_pgroup_name", "ctch_pgroup_text" }, new string[] { "Name", "Text" } ) );
            chat.Items.Add( new NamedColorGroup( "Private messages", new string[] { "ctch_tell_name", "ctch_tell_text" }, new string[] { "Name", "Text" } ) );
            chat.Items.Add( new NamedColorGroup( "OOC", new string[] { "ctch_newbie_name", "ctch_newbie_text" }, new string[] { "Name", "Text" } ) );
            chat.Items.Add( new NamedColorGroup( "Misc.", "This is used various places, among others Org Leaders chat, and Social chat on TestLive", new string[] { "ctch_misc_name", "ctch_misc_text" }, new string[] { "Name", "Text" } ) );
            this.items.Add( chat );

            Category moreChat = new Category( "Chat (cont.)" );
            moreChat.Items.Add( new NamedColorGroup( "Vicinity", new string[] { "ctch_vicinity_name", "ctch_vicinity_text" }, new string[] { "Name", "Text" } ) );
            moreChat.Items.Add( new NamedColorGroup( "Whispers", new string[] { "ctch_whisper_name", "ctch_whisper_text" }, new string[] { "Name", "Text" } ) );
            moreChat.Items.Add( new NamedColorGroup( "Shouts", new string[] { "ctch_shout_name", "ctch_shout_text" }, new string[] { "Name", "Text" } ) );
            moreChat.Items.Add( new NamedColorGroup( "Emotes", new string[] { "ctch_emote_name", "ctch_emote_text" }, new string[] { "Name", "Text" } ) );
            moreChat.Items.Add( new NamedColorGroup( "News", new string[] { "ctch_news_name", "ctch_news_text" }, new string[] { "Name", "Text" } ) );
            moreChat.Items.Add( new NamedColorGroup( "Looking for Group", new string[] { "ctch_seekingteam_name", "ctch_seekingteam_text" }, new string[] { "Name", "Text" } ) );
            moreChat.Items.Add( new ColorGroup( "Your pet", new string[] { "ctch_mypet" } ) );
            moreChat.Items.Add( new ColorGroup( "Other pets", new string[] { "ctch_otherpet" } ) );
            this.items.Add( moreChat );
//
//            Category items = new Category( "Items" );
//            items.Items.Add( new ColorGroup( "Mundane", new string[] { "mundane" } ) );
//            items.Items.Add( new ColorGroup( "Superior", new string[] { "superior" } ) );
//            items.Items.Add( new ColorGroup( "Enchanted (World Drop)", new string[] { "enchantedlight" } ) );
//            items.Items.Add( new ColorGroup( "Enchanted", new string[] { "enchanted" } ) );
//            items.Items.Add( new ColorGroup( "Rare (World Drop)", new string[] { "reliclight" } ) );
//            items.Items.Add( new ColorGroup( "Rare", new string[] { "rare" } ) );
//            items.Items.Add( new ColorGroup( "Epic (World Drop)", new string[] { "epiclight" } ) );
//            items.Items.Add( new ColorGroup( "Epic", new string[] { "epic" } ) );
//            items.Items.Add( new ColorGroup( "Legendary", new string[] { "legendary" } ) );
//            this.items.Add( items );
//
//            Category money = new Category( "Money" );
//            money.Items.Add( new ColorGroup( "Tin", new string[] { "money_tin" } ) );
//            money.Items.Add( new ColorGroup( "Copper", new string[] { "money_copper" } ) );
//            money.Items.Add( new ColorGroup( "Silver", new string[] { "money_silver" } ) );
//            money.Items.Add( new ColorGroup( "Gold", new string[] { "money_gold" } ) );
//            this.items.Add( money );

            Category combatSelf = new Category( "Combat (Self)" );
            combatSelf.Items.Add( new ColorGroup( "Normal hit", new string[] { "self_attacked" } ) );
            combatSelf.Items.Add( new ColorGroup( "Critical hit", new string[] { "self_attacked_critical" } ) );
            combatSelf.Items.Add( new ColorGroup( "Spell: Normal hit", new string[] { "self_attacked_spell" } ) );
            combatSelf.Items.Add( new ColorGroup( "Combo: Normal hit", new string[] { "self_attacked_combo" } ) );
            combatSelf.Items.Add( new ColorGroup( "Environment damage", new string[] { "self_attacked_environment" } ) );
            combatSelf.Items.Add( new ColorGroup( "Dodged", new string[] { "self_dodged" } ) );
            combatSelf.Items.Add( new ColorGroup( "Healed", new string[] { "self_healed" } ) );
            this.items.Add( combatSelf );

            Category combatOther = new Category( "Combat (Other)" );
            combatOther.Items.Add( new ColorGroup( "Normal hit", new string[] { "other_attacked" } ) );
            combatOther.Items.Add( new ColorGroup( "Critical hit", new string[] { "other_attacked_critical" } ) );
            combatOther.Items.Add( new ColorGroup( "Spell: Normal hit", new string[] { "other_attacked_spell" } ) );
            combatOther.Items.Add( new ColorGroup( "Combo: Normal hit", new string[] { "other_attacked_combo" } ) );
            combatOther.Items.Add( new ColorGroup( "Environment damage", new string[] { "other_attacked_environment" } ) );
            combatOther.Items.Add( new ColorGroup( "Dodged", new string[] { "other_dodged" } ) );
            combatOther.Items.Add( new ColorGroup( "Healed", new string[] { "other_healed" } ) );
            this.items.Add( combatOther );
            
            Category misc = new Category( "Misc" );
            misc.Items.Add( new NamedColorGroup( "XP feedback", new string[] { "ctch_megotxp_name", "ctch_megotxp_text" }, new string[] { "Name", "Text" } ) );
            misc.Items.Add( new NamedColorGroup( "Me hit...", new string[] { "ctch_mehit_name", "ctch_mehit_text" }, new string[] { "Name", "Text" } ) );
            misc.Items.Add( new NamedColorGroup( "Me healed...", new string[] { "ctch_mehealed_name", "ctch_mehealed_text" }, new string[] { "Name", "Text" } ) );
            misc.Items.Add( new NamedColorGroup( "GM chat", new string[] { "ctch_gm_name", "ctch_gm_text" }, new string[] { "Name", "Text" } ) );
            misc.Items.Add( new ColorGroup( "System messages", new string[] { "ct_system" } ) );
            this.items.Add( misc );
        }

        private List<Category> items = new List<Category>();
        public List<Category> Items
        {
            get { return this.items; }
        }
    }
}
