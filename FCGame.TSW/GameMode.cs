﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using System.Text.RegularExpressions;

using Twp.FC.AoC;
using Twp.Utilities;

namespace FCGame.TSW
{
    public class GameMode : IGameMode
    {
        #region Properties

        public string Name
        {
            get { return "The Secret World"; }
        }

        public string GameID
        {
            get { return "TSW"; }
        }

        private Settings settings = new Settings();

        public string Path
        {
            get { return this.settings.GamePath; }
        }

        private Gui gui;

        public Twp.FC.XmlFile XmlFile
        {
            get { return this.gui.TextColors; }
        }

        private TextColors colors = new TextColors();
        public TextColors Colors
        {
            get { return this.colors; }
        }

        private ColorCategories categories;
        public IColorCategories Categories
        {
            get { return this.categories; }
        }
                
        public bool SupportsSplitColors
        {
            get { return true; }
        }

        public System.Resources.ResourceManager Resources
        {
            get { return Properties.Resources.ResourceManager; }
        }

        #endregion

        #region Methods

        public bool Initialize( IniSection settings )
        {
            this.settings.Init( settings );
            this.categories = new ColorCategories();

            Log.Debug( "[GameMode.Initialize] Verifying game path." );
            string path = this.settings.GamePath;
            if( !GamePath.Confirm( ref path ) )
                return false;

            this.settings.GamePath = path;
            this.gui = new Gui( path );
            if( !this.gui.TextColors.Exists )
            {
                Log.Debug( "[GameMode.Initialize] No costumized TextColors.xml file found. Using default file." );
                this.InitDefaults( path );
            }
            else
            {
                Log.Debug( "[GameMode.Initialize] Verifying Customized TextColors.xml file." );
                // Check file revisions, and read default file if they don't match.
                Gui defGui = Gui.Default( path );
                int defaultRev = this.ReadRevision( defGui.TextColors.FileName );
                if( defaultRev == -1 )
                    return false;

                int customRev = this.ReadRevision( this.gui.TextColors.FileName );

                Log.Debug( "[GameMode.Initialize] Default: {0} Custom: {1}", defaultRev, customRev );
                if( customRev != defaultRev )
                {
                    Log.Debug( "[GameMode.Initialize] Costumized TextColors.xml file is outdated. Merging with default file." );
                    TextColors oldColors = new TextColors();
                    TextColors newColors = new TextColors();
                    
                    oldColors.Load( this.XmlFile );
                    newColors.Load( defGui.TextColors );
                    
                    foreach( TextColor color in oldColors )
                    {
                        newColors[color.Name].Color = color.Color;
                    }
                    newColors.Save( this.XmlFile.FileName );
                }
                else
                    Log.Debug( "[GameMode.Initialize] Customized file is OK." );
            }

            Log.Debug( "[GameMode.Initialize] Loading TextColors." );
            this.colors.Load( this.XmlFile );
            this.colors.DefaultFileName = Gui.Default( path ).TextColors.FileName;

            Log.Debug( "[GameMode.Initialize] Done." );
            return true;
        }

        private int ReadRevision( string fileName )
        {
            if( !File.Exists( fileName ) )
                return -1;

            Log.Debug( "[GameMode.ReadRevision] Checking File: {0}", fileName );

            Regex change = new Regex( @"\$Change(: [0-9]+ )?\$" );
            using( StreamReader sr = File.OpenText( fileName ) )
            {
                Char[] buffer = new Char[200];
                sr.Read( buffer, 0, buffer.Length );
                String input = new String( buffer );
//                Log.Debug( "[GameMode.ReadRevision] Buffer: {0}", input );
                Match match = change.Match( input );
                if( match.Success )
                {
                    Log.Debug( "[GameMode.ReadRevision] '{0}' was found at {1}", match.Value, match.Index + 1 );

                    string value = match.Value.Substring( 8, match.Value.Length - 10 );
                    return Convert.ToInt32( value.Trim() );
                }
            }
            Log.Debug( "[GameMode.ReadRevision] No Matches found!" );
            return -1;
        }

        private void InitDefaults( string path )
        {
            Gui defGui = Gui.Default( path );
            string fileName = this.gui.TextColors.FileName;
            this.gui.TextColors.Load( defGui.TextColors.FileName );
            this.gui.TextColors.FileName = fileName;
        }

        public void Cleanup()
        {
            this.categories.Items.Clear();
            this.categories = null;
        }

        public void Dispose()
        {
            this.gui = null;
        }

        public bool Browse()
        {
            string path = this.settings.GamePath;
            if( !GamePath.Confirm( ref path ) )
                return false;

            this.settings.GamePath = path;
            return true;
        }

        public override string ToString()
        {
            return this.Name;
        }

        #endregion

        #region Unsupported

        public bool SupportsGuiChange
        {
            get { return false; }
        }

        public string[] AvailableGuis
        {
            get { throw new NotSupportedException(); }
        }

        public string CurrentGui
        {
            get { throw new NotSupportedException(); }
            set { throw new NotSupportedException(); }
        }

        public bool WritePrefs()
        {
            throw new NotImplementedException();
        }
        
        public bool CreateGui( string name )
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
