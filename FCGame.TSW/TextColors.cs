﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2012
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.Collections.Generic;

namespace FCGame.AoC
{
    public class TextColors : ITextColors
    {
        public TextColors()
        {
            Category chat = new Category( "Chat" );
            chat.Items.Add( new ColorGroup( "Org. chat", new string[] { "ctch_clan", "CCClanColor" } ) );
            chat.Items.Add( new ColorGroup( "Raid chat", new string[] { "ctch_raid" } ) );
            chat.Items.Add( new ColorGroup( "Team chat", new string[] { "ctch_team", "CCTeamColor" } ) );
            chat.Items.Add( new ColorGroup( "Private groups", new string[] { "ctch_pgroup" } ) );
            chat.Items.Add( new ColorGroup( "Private messages", new string[] { "ct_itell", "ctch_tell", "CCTellColor" } ) );
            chat.Items.Add( new ColorGroup( "OOC", new string[] { "ctch_newbie", "CCNewbieColor" } ) );
            chat.Items.Add( new ColorGroup( "Misc.", "This is used various places, among others Org Leaders chat, and Social chat on TestLive", new string[] { "ctch_misc", "CCMiscColor" } ) );
            this.categories.Add( chat );

            Category moreChat = new Category( "Chat (cont.)" );
            moreChat.Items.Add( new ColorGroup( "Vicinity", new string[] { "ctch_vicinity", "CCVicinityColor" } ) );
            moreChat.Items.Add( new ColorGroup( "Whispers", new string[] { "ctch_whisper", "CCWhisperColor" } ) );
            moreChat.Items.Add( new ColorGroup( "Shouts", new string[] { "ctch_shout", "CCShoutColor" } ) );
            moreChat.Items.Add( new ColorGroup( "Emotes", new string[] { "ctch_emote", "CCEmoteColor" } ) );
            moreChat.Items.Add( new ColorGroup( "Shopping channels", new string[] { "ctch_seekingteam", "CCSeekingTeamColor" } ) );
            moreChat.Items.Add( new ColorGroup( "Your pet", new string[] { "ctch_mypet" } ) );
            moreChat.Items.Add( new ColorGroup( "Other pets", new string[] { "ctch_otherpet" } ) );
            this.categories.Add( moreChat );

            Category info = new Category( "Info box" );
            info.Items.Add( new ColorGroup( "Headline", new string[] { "CCInfoHeadline" } ) );
            info.Items.Add( new ColorGroup( "Header", new string[] { "CCInfoHeader" } ) );
            info.Items.Add( new ColorGroup( "Normal text", new string[] { "CCInfoText" } ) );
            info.Items.Add( new ColorGroup( "Bold text", new string[] { "CCInfoTextBold" } ) );
            this.categories.Add( info );

            Category npc = new Category( "NPC Chat" );
            npc.Items.Add( new ColorGroup( "NPC chat text", new string[] { "CCNPCChatText" } ) );
            npc.Items.Add( new ColorGroup( "NPC OOC text", new string[] { "CCNPCOOCText" } ) );
            npc.Items.Add( new ColorGroup( "NPC emotes", new string[] { "CCNPCChatEmote" } ) );
            npc.Items.Add( new ColorGroup( "NPC system text", new string[] { "CCNPCChatSystem" } ) );
            npc.Items.Add( new ColorGroup( "Your chat text", new string[] { "CCNPCChatQuestion" } ) );
            npc.Items.Add( new ColorGroup( "Descriptions", new string[] { "CCNPCChatDescription" } ) );
            npc.Items.Add( new ColorGroup( "Trade text", new string[] { "CCNPCChatTrade" } ) );
            this.categories.Add( npc );

            Category combat = new Category( "Combat" );
            combat.Items.Add( new ColorGroup( "You got hit by a monster", new string[] { "CCMonsterHitMeColor" } ) );
            combat.Items.Add( new ColorGroup( "You got hit by a player", new string[] { "CCPlayerHitMeColor" } ) );
            combat.Items.Add( new ColorGroup( "You got hit by nano damage", new string[] { "CCMeHitByNanoColor" } ) );
            combat.Items.Add( new ColorGroup( "You got hit by unknown source", new string[] { "CCMeHitOtherColor" } ) );
            combat.Items.Add( new ColorGroup( "Your pet hit someone", new string[] { "CCOtherHitOtherMyPetColor" } ) );
            combat.Items.Add( new ColorGroup( "Others got hit by nano damage", new string[] { "CCOtherHitByNanoColor" } ) );
            combat.Items.Add( new ColorGroup( "Others got hit by others", new string[] { "CCOtherHitOtherColor" } ) );
            this.categories.Add( combat );

            Category misc = new Category( "Misc." );
            misc.Items.Add( new ColorGroup( "Org. & Tower messages ", new string[] { "ctch_tower", "CCTowerColor" } ) );
            misc.Items.Add( new ColorGroup( "XP/SK feedback", new string[] { "CCMeGotXpColor" } ) );
            misc.Items.Add( new ColorGroup( "Research feedback", new string[] { "ctch_research" } ) );
            misc.Items.Add( new ColorGroup( "Healing feedback", new string[] { "CCMeHealedColor" } ) );
            misc.Items.Add( new ColorGroup( "Skill available", new string[] { "CCSkillColor" } ) );
            //misc.Items.Add( new ColorGroup( "Nano casting feedback", new string[] { "ct_castnano" } ) );
            misc.Items.Add( new ColorGroup( "System messages", new string[] { "ct_system" } ) );
            this.categories.Add( misc );
        }

        private List<Category> categories = new List<Category>();
        public List<Category> Categories
        {
            get { return this.categories; }
        }
    }
}
