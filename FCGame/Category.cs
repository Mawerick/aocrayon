﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace FCGame
{
    public class Category
    {
        public Category()
        {
        }

        public Category( string name )
        {
            this.name = name;
        }

        public Category( string name, ColorGroup[] groups )
        {
            this.name = name;
            this.items.AddRange( groups );
        }

        private string name;
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        private List<ColorGroup> items = new List<ColorGroup>();
        public List<ColorGroup> Items
        {
            get { return this.items; }
        }
    }
}
