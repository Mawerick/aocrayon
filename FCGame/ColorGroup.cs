﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace FCGame
{
    public class ColorGroup
    {
        #region Constructors

        public ColorGroup( string text, string[] colors )
        {
            this.text = text;
            this.colors.AddRange( colors );
        }

        public ColorGroup( string text, string toolTip, string[] colors )
        {
            this.text = text;
            this.toolTip = toolTip;
            this.colors.AddRange( colors );
        }

        #endregion

        #region Properties

        private string text;
        public string Text
        {
            get { return this.text; }
        }

        private string toolTip;
        public string ToolTip
        {
            get { return this.toolTip; }
        }

        private List<string> colors = new List<string>();
        public List<string> Colors
        {
            get { return this.colors; }
        }

        #endregion
    }
}
