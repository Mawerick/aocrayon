﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Resources;
using Twp.Utilities;

namespace FCGame
{
    /// <summary>
    /// 
    /// </summary>
    public interface IGameMode : IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the name of the <see cref="IGameMode" />.
        /// </summary>
        string Name { get; }
        
        /// <summary>
        /// Gets the ID of the <see cref="IGameMode" />.
        /// </summary>
        string GameID { get; }

        /// <summary>
        /// Gets the Game Path of the <see cref="IGameMode" />.
        /// </summary>
        string Path { get; }

        /// <summary>
        /// Gets a boolean value indicating wether or not the <see cref="IGameMode" /> supports changing GUIs.
        /// </summary>
        bool SupportsGuiChange { get; }
        
        /// <summary>
        /// Gets an array representing the names of available GUIs.
        /// </summary>
        string[] AvailableGuis { get; }
        
        /// <summary>
        /// Gets or sets a string representing the current selected GUI.
        /// </summary>
        string CurrentGui { get; set; }

        /// <summary>
        /// 
        /// </summary>
        TextColors Colors { get; }
        
        /// <summary>
        /// 
        /// </summary>
        IColorCategories Categories { get; }
        
        /// <summary>
        /// Gets a boolean value indicating wether or not the <see cref="IGameMode" /> supports setting different colors for the elements of a message.
        /// </summary>
        bool SupportsSplitColors { get; }

        /// <summary>
        /// 
        /// </summary>
        ResourceManager Resources { get; }

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        bool Initialize( IniSection settings );
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool WritePrefs();
        
        /// <summary>
        /// 
        /// </summary>
        void Cleanup();
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool Browse();
        
        /// <summary>
        /// Creates a new GUI folder with the specified name.
        /// </summary>
        bool CreateGui( string name );

        #endregion
    }
}
