﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;

namespace FCGame
{
    /// <summary>
    /// Description of NamedColorGroup.
    /// </summary>
    public class NamedColorGroup : ColorGroup
    {
        public NamedColorGroup( string text, string[] colors, string[] names ) : base( text, colors )
        {
            this.names = names;
        }

        public NamedColorGroup( string text, string toolTip, string[] colors, string[] names ) : base( text, colors )
        {
            this.names = names;
        }
        
        private string[] names;
        public string[] Names
        {
            get { return this.names; }
        }
    }
}
