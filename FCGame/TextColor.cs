﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Xml;

namespace FCGame
{
    /// <summary>
    /// Description of TextColor.
    /// </summary>
    public class TextColor : INotifyPropertyChanged
    {
        public TextColor( XmlElement element )
        {
            this.element = element;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged( string property )
        {
            if( this.PropertyChanged != null )
                this.PropertyChanged( this, new PropertyChangedEventArgs( property ) );
        }

        protected XmlElement element;

        private XmlNode nameNode;
        public string Name
        {
            get
            {
                if( this.nameNode == null )
                    this.nameNode = this.GetXmlAttribute( "name" );
                return this.nameNode.Value;
            }
        }

        private XmlNode colorNode;
        private Color color = Color.Empty;
        public Color Color
        {
            get
            {
                if( this.color == Color.Empty )
                {
                    this.ColorFromXml( this.ColorName );
                }
                return this.color;
            }
            set
            {
                if( this.color != value )
                {
                    this.color = value;
                    if( this.colorNode == null )
                        this.colorNode = this.GetXmlAttribute( "color" );
                    this.ColorToXml();
                    this.OnPropertyChanged( "Color" );
                }
            }
        }

        public string ColorName
        {
            get
            {
                if( this.colorNode == null )
                    this.colorNode = this.GetXmlAttribute( "color" );
                return this.colorNode.Value;
            }
        }

        private ColorType type = ColorType.String;
        private enum ColorType
        {
            String,
            Hex3,
            Hex4,
        }

        public void ColorFromXml( string color )
        {
            if( color.StartsWith( "0x" ) )
            {
                int r, g, b;
                string hex = color.Substring( 2 );
                if( hex.Length > 6 )
                {
                    this.type = ColorType.Hex4;
                    int pad = hex.Length - 6;
                    r = Convert.ToByte( hex.Substring( pad, 2 ), 16 );
                    g = Convert.ToByte( hex.Substring( 2 + pad, 2 ), 16 );
                    b = Convert.ToByte( hex.Substring( 4 + pad, 2 ), 16 );
                }
                else
                {
                    this.type = ColorType.Hex3;
                    r = Convert.ToByte( hex.Substring( 0, 2 ), 16 );
                    g = Convert.ToByte( hex.Substring( 2, 2 ), 16 );
                    b = Convert.ToByte( hex.Substring( 4, 2 ), 16 );
                }
                this.color = Color.FromArgb( r, g, b );
            }
            else if( color.StartsWith( "#" ) )
            {
                int r, g, b;
                this.type = ColorType.Hex3;
                string hex = color.Substring( 1 );
                r = Convert.ToByte( hex.Substring( 0, 2 ), 16 );
                g = Convert.ToByte( hex.Substring( 2, 2 ), 16 );
                b = Convert.ToByte( hex.Substring( 4, 2 ), 16 );
                this.color = Color.FromArgb( r, g, b );
            }
            else
            {
                this.type = ColorType.String;
                this.color = Color.Empty; //Color.FromName( color );
            }
        }
        
        public void ColorToXml()
        {
            if( this.type == ColorType.String )
            {
                if( this.color.IsNamedColor )
                    this.colorNode.Value = this.color.Name.ToLowerInvariant();
                this.type = ColorType.Hex4;
            }

            byte[] vals = new byte[] { this.color.R, this.color.G, this.color.B };
            string ret = BitConverter.ToString( vals ).Replace( "-", "" );
            if( this.type == ColorType.Hex3 )
                this.colorNode.Value = "0x" + ret;
            else
                this.colorNode.Value = "0x00" + ret;
        }

        protected XmlNode GetXmlAttribute( string name )
        {
            return this.element.Attributes.GetNamedItem( name );
        }
    }
}
