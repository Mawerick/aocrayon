﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;

using Twp.FC;
using Twp.Utilities;

namespace FCGame
{
    /// <summary>
    /// Description of TextColors.
    /// </summary>
    public class TextColors : BindingList<TextColor>
    {
        private bool loaded = false;
        public bool Loaded
        {
            get { return this.loaded; }
        }
        
        public event EventHandler LoadFinished;
        
        private bool hasColors = false;
        public bool HasColors
        {
            get { return this.hasColors; }
        }

        private bool hasFonts = false;
        public bool HasFonts
        {
            get { return this.hasFonts; }
        }
        
        public TextColor this[string name]
        {
            get
            {
                foreach( TextColor color in this )
                {
                    if( color.Name == name )
                        return color;
                }
                return null;
            }
        }

        private XmlFile xmlFile;

        public bool Add( string name )
        {
            if( this.xmlFile == null )
                return false;

            XmlElement element = this.xmlFile.CreateElement( "HTMLColor" );
            element.SetAttribute( "name", name );
            element.SetAttribute( "color", "white" );
            this.xmlFile.DocumentElement.AppendChild( element );
            this.xmlFile.Debug();
            TextColor color = new TextColor( element );
            this.Add( color );

            return true;
        }

        public void Load( string fileName )
        {
            this.xmlFile = new XmlFile();
            this.xmlFile.Load( fileName );
            this.Load();
        }

        public void Load( XmlFile xmlFile )
        {
            this.xmlFile = xmlFile;
            this.Load();
        }

        private void Load()
        {
            this.loaded = false;
            this.Clear();

            XmlNodeList nodes = this.xmlFile.GetElementsByTagName( "HTMLColor" );
            if( nodes != null && nodes.Count > 0 )
            {
                Log.Debug( "[TextColors.Load] {0} HTMLColor tags found.", nodes.Count );
                foreach( XmlElement element in nodes )
                {
                    TextColor color = new TextColor( element );
                    this.Add( color );
                }
                this.hasColors = true;
            }
            else
            {
                Log.Debug( "[TextColors.Load] No HTMLColor tags found." );
                this.hasColors = false;
            }

            nodes = this.xmlFile.GetElementsByTagName( "HTMLFont" );
            if( nodes != null && nodes.Count > 0 )
            {
                Log.Debug( "[TextColors.Load] {0} HTMLFont tags found.", nodes.Count );
                foreach( XmlElement element in nodes )
                {
                    TextFont font = new TextFont( element );
                    this.Add( font );
                }
                this.hasFonts = true;
            }
            else
            {
                Log.Debug( "[TextColors.Load] No HTMLFont tags found." );
                this.hasFonts = false;
            }

            this.loaded = true;
            if( this.LoadFinished != null )
                this.LoadFinished( this, EventArgs.Empty );
        }
        
        /// <summary>
        /// Save the text colors to a new file. No backup is made.
        /// </summary>
        /// <param name="fileName"></param>
        public void Save( string fileName )
        {
            this.xmlFile.FileName = fileName;
            this.xmlFile.Save();
        }
        
        public void Save( ProgressBar progressBar )
        {
            if( progressBar != null )
            {
                progressBar.Minimum = 0;
                progressBar.Maximum = 2;
                progressBar.Value = 0;
                progressBar.Step = 1;
            }
            this.Backup( true );
            if( progressBar != null )
                progressBar.PerformStep();
            this.xmlFile.Save();
            if( progressBar != null )
                progressBar.PerformStep();
        }
        
        private string BackupFileName
        {
            get
            {
                System.IO.FileInfo fi = new System.IO.FileInfo( this.xmlFile.FileName );
                return System.IO.Path.Combine( fi.DirectoryName, Application.ProductName + "_" + fi.Name );
            }
        }

        private string defaultFileName;
        public string DefaultFileName
        {
            get { return this.defaultFileName; }
            set { this.defaultFileName = value; }
        }

        public void Backup()
        {
            this.Backup( false );
        }

        public void Backup( bool force )
        {
            if( System.IO.File.Exists( this.xmlFile.FileName ) )
                System.IO.File.Copy( this.xmlFile.FileName, this.BackupFileName, force );
        }

        public void Restore()
        {
            if( !System.IO.File.Exists( this.BackupFileName ) )
            {
                Twp.Utilities.Log.Warning( Properties.Resources.RestoreFileMissing, Properties.Resources.RestoreLast );
                Twp.Utilities.Log.Debug( "[TextColors.Restore] File: {0}", this.BackupFileName );
                return;
            }

            DialogResult dr = MessageBox.Show( String.Format( Properties.Resources.RestoreText, Properties.Resources.RestoreLast ),
                                               Properties.Resources.RestoreCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning );
            if( dr == DialogResult.Yes )
            {
                System.IO.File.Copy( this.BackupFileName, this.xmlFile.FileName, true );
                this.Load( this.xmlFile.FileName );
            }
        }
        
        public void RestoreDefaults()
        {
            if( !System.IO.File.Exists( this.DefaultFileName ) )
            {
                Twp.Utilities.Log.Warning( Properties.Resources.RestoreFileMissing, Properties.Resources.RestoreDefaults );
                Twp.Utilities.Log.Debug( "[TextColors.RestoreDefaults] File: {0}", this.DefaultFileName );
                return;
            }

            DialogResult dr = MessageBox.Show( String.Format( Properties.Resources.RestoreText, Properties.Resources.RestoreDefaults ),
                                               Properties.Resources.RestoreCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning );
            if( dr == DialogResult.Yes )
            {
                Twp.Utilities.Log.Debug( "[TextColors.RestoreDefaults] Copying {0} to {1}", this.DefaultFileName, this.xmlFile.FileName );
                System.IO.File.Copy( this.DefaultFileName, this.xmlFile.FileName, true );
                this.Load( this.xmlFile.FileName );
            }
        }
    }
}
