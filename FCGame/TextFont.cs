﻿// $Id$
//
//  Copyright (C) Mawerick, WrongPlace.Net 2005 - 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Globalization;
using System.Xml;

namespace FCGame
{
    public class TextFont : TextColor
    {
        public TextFont( XmlElement element ) : base( element )
        {
        }

        private XmlNode fontSizeNode;
        public string FontSize
        {
            get
            {
                if( this.fontSizeNode == null )
                    this.fontSizeNode = this.GetXmlAttribute( "font-size" );
                return this.fontSizeNode.Value;
            }
            set
            {
                if( this.fontSizeNode == null )
                    this.fontSizeNode = this.GetXmlAttribute( "font-size" );
                if( this.fontSizeNode.Value != value )
                {
                    this.fontSizeNode.Value = value;
                    this.OnPropertyChanged( "FontSize" );
                }
            }
        }

        private XmlNode fontStyleNode;
        public string FontStyle
        {
            get
            {
                if( this.fontStyleNode == null )
                    this.fontStyleNode = this.GetXmlAttribute( "font-style" );
                return this.fontStyleNode.Value;
            }
            set
            {
                if( this.fontStyleNode == null )
                    this.fontStyleNode = this.GetXmlAttribute( "font-style" );
                if( this.fontStyleNode.Value != value )
                {
                    this.fontStyleNode.Value = value;
                    this.OnPropertyChanged( "FontStyle" );
                }
            }
        }

        private XmlNode fontFamilyNode;
        public string FontFamily
        {
            get
            {
                if( this.fontFamilyNode == null )
                    this.fontFamilyNode = this.GetXmlAttribute( "font-family" );
                return this.fontFamilyNode.Value;
            }
            set
            {
                if( this.fontFamilyNode == null )
                    this.fontFamilyNode = this.GetXmlAttribute( "font-family" );
                if( this.fontFamilyNode.Value != value )
                {
                    this.fontFamilyNode.Value = value;
                    this.OnPropertyChanged( "FontFamily" );
                }
            }
        }

        private XmlNode speedNode;
        private int speed;
        public int Speed
        {
            get
            {
                if( this.speed == 0 )
                {
                    if( this.speedNode == null )
                        this.speedNode = this.GetXmlAttribute( "speed" );
                    this.speed = Convert.ToInt32( this.speedNode.Value );
                }
                return this.speed;
            }
            set
            {
                if( this.speed != value )
                {
                    this.speed = value;
                    if( this.speedNode == null )
                        this.speedNode = this.GetXmlAttribute( "speed" );
                    this.speedNode.Value = value.ToString();
                    this.OnPropertyChanged( "Speed" );
                }
            }
        }

        private XmlNode waitOnScreenNode;
        private decimal waitOnScreen;
        public decimal WaitOnScreen
        {
            get
            {
                if( this.waitOnScreen == 0 )
                {
                    if( this.waitOnScreenNode == null )
                        this.waitOnScreenNode = this.GetXmlAttribute( "waitonscreen" );
                    this.waitOnScreen = Convert.ToDecimal( this.waitOnScreenNode.Value, NumberFormatInfo.InvariantInfo );
                }
                return this.waitOnScreen;
            }
            set
            {
                if( this.waitOnScreen != value )
                {
                    this.waitOnScreen = value;
                    if( this.waitOnScreenNode == null )
                        this.waitOnScreenNode = this.GetXmlAttribute( "waitonscreen" );
                    this.waitOnScreenNode.Value = value.ToString( NumberFormatInfo.InvariantInfo );
                    this.OnPropertyChanged( "WaitOnScreen" );
                }
            }
        }

        private XmlNode directionNode;
        private int direction;
        public int Direction
        {
            get
            {
                if( this.direction == 0 )
                {
                    if( this.directionNode == null )
                        this.directionNode = this.GetXmlAttribute( "direction" );
                    this.direction = Convert.ToInt32( this.directionNode.Value );
                }
                return this.direction;
            }
            set
            {
                if( this.direction != value )
                {
                    this.direction = value;
                    if( this.directionNode == null )
                        this.directionNode = this.GetXmlAttribute( "direction" );
                    this.directionNode.Value = value.ToString();
                    this.OnPropertyChanged( "Direction" );
                }
            }
        }
    }
}
